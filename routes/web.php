<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', ['uses' => 'HomeController@index', 'as' => 'front.home.index']);


Route::get('/kompanija', ['uses' => 'HomeController@company', 'as' => 'front.company.index']);
Route::get('/kompanija/{slug}', ['uses' => 'HomeController@showCompany', 'as' => 'front.company.show']);

Route::get('/proizvodi', ['uses' => 'HomeController@products', 'as' => 'front.product.index']);
Route::get('/proizvodi/{cat}', ['uses' => 'HomeController@productList', 'as' => 'front.product.list']);
Route::get('/proizvodi/{cat}/{slug}', ['uses' => 'HomeController@productShow', 'as' => 'front.product.show']);

Route::get('/rental', ['uses' => 'HomeController@rental', 'as' => 'front.rental.list']);
Route::get('/rental/{slug}', ['uses' => 'HomeController@showRental', 'as' => 'front.rental.show']);

Route::get('/polovne-masine', ['uses' => 'HomeController@used', 'as' => 'front.used.list']);
Route::get('/polovne-masine/{slug}', ['uses' => 'HomeController@showUsed', 'as' => 'front.used.show']);

Route::get('/servis-i-delovi', ['uses' => 'HomeController@showService', 'as' => 'front.service.show']);

Route::get('/vesti', ['uses' => 'HomeController@post', 'as' => 'front.post.index']);
Route::get('/vesti/{slug}', ['uses' => 'HomeController@showPost', 'as' => 'front.post.show']);

Route::get('/galerija', ['uses' => 'HomeController@gallery', 'as' => 'front.gallery.index']);
Route::get('/kontakt', ['uses' => 'HomeController@contact', 'as' => 'front.contact.index']);
Route::get('/zaposlenje', ['uses' => 'HomeController@career', 'as' => 'front.career.index']);

Route::post('/send', ['uses' => 'HomeController@contactUs', 'as' => 'front.contact.send']);

// Route::get('/proizvodi', ['uses' => 'HomeController@products', 'as' => 'front.product.index']);


Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.'], function() {
    Route::get('/login', ['uses' => 'AuthController@login', 'as' => 'auth.login']);
    Route::post('/login', ['uses' => 'AuthController@postLogin', 'as' => 'auth.post-login']);
    Route::get('/logout', ['uses' => 'AuthController@logout', 'as' => 'auth.logout']);
    
    Route::group(['middleware' => 'auth.admin'], function () {
        Route::get('/', ['uses' => 'DashboardController@index', 'as' => 'dashboard.index']);
        
        Route::post('/administrator/change-status', ['uses' => 'AdministratorController@changeState', 'as' => 'administrator.change-status']);
        Route::resource('administrator','AdministratorController');
        
        Route::post('/slider/change-status', ['uses' => 'SliderController@changeState', 'as' => 'slider.change-status']);
        Route::put('/slider/reorder', ['uses' => 'SliderController@reorder', 'as' => 'slider.reorder']);
        Route::resource('slider','SliderController');
        
        Route::post('/post/change-status', ['uses' => 'PostController@changeState', 'as' => 'post.change-status']);
        Route::put('/post/reorder', ['uses' => 'PostController@reorder', 'as' => 'post.reorder']);
        Route::post('/post/slugify', ['uses' => 'PostController@slugify', 'as' => 'post.slugify']);
        Route::resource('post', 'PostController');
        
        Route::post('/post-photo/{id}', ['uses' => 'PostPhotoController@store', 'as' => 'post-photo.store']);
        Route::put('/post-photo/change-status', ['uses' => 'PostPhotoController@changeState', 'as' => 'post-photo.change-status']);
        Route::put('/post-photo/reorder', ['uses' => 'PostPhotoController@reorder', 'as' => 'post-photo.reorder']);
        Route::delete('/post-photo/{id}', ['uses' => 'PostPhotoController@destroy', 'as' => 'post-photo.destroy']);
        
        Route::post('/post-category/change-status', ['uses' => 'PostCategoryController@changeState', 'as' => 'post-category.change-status']);
        Route::put('/post-category/reorder', ['uses' => 'PostCategoryController@reorder', 'as' => 'post-category.reorder']);
        Route::post('/post-category/slugify', ['uses' => 'PostCategoryController@slugify', 'as' => 'post-category.slugify']);
        Route::resource('post-category', 'PostCategoryController');
        
        Route::post('/product/change-status', ['uses' => 'ProductController@changeState', 'as' => 'product.change-status']);
        Route::put('/product/reorder', ['uses' => 'ProductController@reorder', 'as' => 'product.reorder']);
        Route::post('/product/slugify', ['uses' => 'ProductController@slugify', 'as' => 'product.slugify']);
        Route::resource('product', 'ProductController');
        
        Route::post('/specification/change-status', ['uses' => 'SpecificationController@changeState', 'as' => 'specification.change-status']);
        Route::resource('specification', 'SpecificationController');

        Route::post('/product-photo/{id}', ['uses' => 'ProductPhotoController@store', 'as' => 'product-photo.store']);        
        Route::put('/product-photo/change-status', ['uses' => 'ProductPhotoController@changeState', 'as' => 'product-photo.change-status']);
        Route::put('/product-photo/reorder', ['uses' => 'ProductPhotoController@reorder', 'as' => 'product-photo.reorder']);
        Route::delete('/product-photo/{id}', ['uses' => 'ProductPhotoController@destroy', 'as' => 'product-photo.destroy']);


        Route::post('/product-category/change-status', ['uses' => 'ProductCategoryController@changeState', 'as' => 'product-category.change-status']);
        Route::put('/product-category/reorder', ['uses' => 'ProductCategoryController@reorder', 'as' => 'product-category.reorder']);
        Route::post('/product-category/slugify', ['uses' => 'ProductCategoryController@slugify', 'as' => 'product-category.slugify']);
        Route::resource('product-category', 'ProductCategoryController');
        
        Route::post('/product-subcategory/change-status', ['uses' => 'ProductSubcategoryController@changeState', 'as' => 'product-subcategory.change-status']);
        Route::put('/product-subcategory/reorder', ['uses' => 'ProductSubcategoryController@reorder', 'as' => 'product-subcategory.reorder']);
        Route::post('/product-subcategory/slugify', ['uses' => 'ProductSubcategoryController@slugify', 'as' => 'product-subcategory.slugify']);
        Route::resource('product-subcategory', 'ProductSubcategoryController');
        
        Route::post('/associate/change-status', ['uses' => 'AssociateController@changeState', 'as' => 'associate.change-status']);
        Route::put('/associate/reorder', ['uses' => 'AssociateController@reorder', 'as' => 'associate.reorder']);
        Route::post('/associate/slugify', ['uses' => 'AssociateController@slugify', 'as' => 'associate.slugify']);
        Route::resource('associate', 'AssociateController');
        
        
        Route::post('/gallery/change-status', ['uses' => 'GalleryController@changeState', 'as' => 'gallery.change-status']);
        Route::post('/gallery/slugify', ['uses' => 'GalleryController@slugify', 'as' => 'gallery.slugify']);
        Route::resource('gallery', 'GalleryController');


        Route::post('/gallery-photo/{id}', ['uses' => 'GalleryPhotoController@store', 'as' => 'gallery-photo.store']);
        Route::put('/gallery-photo/change-status', ['uses' => 'GalleryPhotoController@changeState', 'as' => 'gallery-photo.change-status']);
        Route::put('/gallery-photo/reorder', ['uses' => 'GalleryPhotoController@reorder', 'as' => 'gallery-photo.reorder']);
        Route::delete('/gallery-photo/{id}', ['uses' => 'GalleryPhotoController@destroy', 'as' => 'gallery-photo.destroy']);
        
        Route::post('/service/change-status', ['uses' => 'ServiceController@changeState', 'as' => 'service.change-status']);
        Route::put('/service/reorder', ['uses' => 'ServiceController@reorder', 'as' => 'service.reorder']);
        Route::resource('service', 'ServiceController');

    });
});