<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostCategory extends Model
{
    protected $table = 'post_categories';

    protected $fillable = ['name', 'slug', 'order', 'is_active'];

    public $timestamps = false;

    public function posts()
    {
        return $this->hasMany('App\Post');
    } 
}
