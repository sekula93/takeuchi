<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'slug' => 'required', 
            'subcategory_id' => 'required',
            // 'text' => 'required',
            // 'file' => 'required',
            'is_active' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Obavezno polje',
            'slug.required' => 'Obavezno polje',
            'subcategory_id.required' => 'Obavezno polje',
            // 'text.required' => 'Obavezno polje',
            // 'file.required' => 'Obavezno polje',
            'is_active.required'  => 'Obavezno polje',
        ];
    }
}
