<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'slug' => 'required', 
            'post_category_id' => 'required',
            // 'excerpt' => 'required',
            'text' => 'required',
            // 'file' => 'required',
            // 'post_date' => 'required',
            'is_active' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Obavezno polje',
            'slug.required' => 'Obavezno polje',
            'post_category_id.required' => 'Obavezno polje',
            // 'excerpt.required' => 'Obavezno polje',
            'text.required' => 'Obavezno polje',
            // 'file.required' => 'Obavezno polje',
            // 'post_date.required' => 'Obavezno polje',
            'is_active.required'  => 'Obavezno polje',
        ];
    }
}
