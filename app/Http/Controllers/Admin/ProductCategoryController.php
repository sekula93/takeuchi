<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\File\Image as ImageService;
use App\Repositories\ProductCategory\ProductCategoryInterface;
use App\Http\Requests\ProductCategoryRequest;

class ProductCategoryController extends BaseController
{
    protected $category;

    public function __construct(ProductCategoryInterface $category)
    {
        $this->category = $category;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listArray = $this->category->all();
        return view('admin.product.category.index', compact('listArray'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductCategoryRequest $request, ImageService $media)
    {
        $data = $request->all();
        $file = $request->file('file');

        if ($file && in_array($file->guessExtension(), ['jpeg', 'png'])) {
            $data['filename'] = $media->upload($file, config('settings.image.product-category'));
        }
        $this->category->create($data);
        return redirect(route('admin.product-category.index'));
        \flash('Kategorija je uspešno kreirana')->success();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->category->find($id);
        return view('admin.product.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductCategoryRequest $request, ImageService $media, $id)
    {
        $category = $this->category->find($id);
        $data = $request->all();

        $data['filename'] = $media->upload($request->file('file'), config('settings.image.product-category'));
        // $data['image'] = $image->upload($request->file('image'), config('settings.image.post.featured'));
        // $data['landscape_image'] = $image->upload($request->file('landscape_image'), config('settings.image.post.landscape'));
        if ($data['filename']) {
            $media->delete($this->category->find($id)->filename, config('settings.image.product-category'));
        }
        // if ($data['image']) {
        //     $image->delete($this->post->find($id)->image, config('settings.image.post.featured'));
        // }
        // if ($data['landscape_image']) {
        //     $image->delete($this->post->find($id)->landscape_image, config('settings.image.post.landscape'));
        // }
        $this->category->update($id, $data);

        \flash('Kategorija je uspešno izmenjena')->success();
        return redirect(route('admin.product-category.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->deleteItem($this->category, $id);
    }

    public function changeState(Request $request)
    {
        return $this->changeStatus($this->category, $request->id);
    }

    public function slugify(Request $request)
    {
        return ['slug' => $this->category->slugify($request->all())];
    }

    public function reorder(Request $request)
    {
        if ($request->get('sort_item')) {
            foreach ($request->get('sort_item') as $index => $itemId) {
                $this->category->setOrder($itemId, $index);
            }
            return ['status' => 'success'];
        }
    }
}
