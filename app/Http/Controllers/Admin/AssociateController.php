<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AssociateRequest;
use App\Repositories\Associate\AssociateInterface;
use App\Services\File\Image as ImageService;

class AssociateController extends BaseController
{

    protected $associate;

    public function __construct(AssociateInterface $associate)
    {
        $this->associate = $associate;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listArray = $this->associate->all();
        return view('admin.associate.index', compact('listArray'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.associate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ImageService $media, AssociateRequest $request)
    {
        $data = $request->all();
        $file = $request->file('file');
        if (in_array($file->guessExtension(), ['jpeg', 'png'])) {
            $data['filename'] = $media->upload($file, config('settings.image.associates'));
        }
        $this->associate->create($data);
        return redirect(route('admin.associate.index'));
        \flash('Saradnik je uspešno kreiran')->success();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $associate = $this->associate->find($id);
        return view('admin.associate.edit', compact('associate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ImageService $media, AssociateRequest $request, $id)
    {
        $associate = $this->associate->find($id);
        $data = $request->all();

        $data['filename'] = $media->upload($request->file('file'), config('settings.image.associates'));
        if ($data['filename']) {
            $media->delete($this->associate->find($id)->filename, config('settings.image.associates'));
        }
        $this->associate->update($id, $data);

        \flash('Post je uspešno izmenjen')->success();
        return redirect(route('admin.associate.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->deleteItem($this->associate, $id);
    }

    public function changeState(Request $request)
    {
        return $this->changeStatus($this->associate, $request->id);
    }

    public function slugify(Request $request)
    {
        return ['slug' => $this->associate->slugify($request->all())];
    }

    public function reorder(Request $request)
    {
        if ($request->get('sort_item')) {
            foreach ($request->get('sort_item') as $index => $itemId) {
                $this->associate->setOrder($itemId, $index);
            }
            return ['status' => 'success'];
        }
    }
}
