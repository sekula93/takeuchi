<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    public function changeStatus($model, $id)
    {
        $result = $model->changeStatus($id);
        return $result === false ? ['status' => 'error'] : ['status' => 'success', 'state' => $result];
    }

    public function deleteItem($model, $id)
    {
        return $model->delete($id) ? ['status' => 'success'] : ['status' => 'error'];
    }
}
