<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Services\File\Image as ImageService;
use App\Services\File\Document as DocumentService;
use App\Repositories\Product\ProductInterface;
use App\Repositories\ProductSubcategory\ProductSubcategoryInterface as Subcategory;

class ProductController extends BaseController
{
    protected $product;

    public function __construct(ProductInterface $product) {
        $this->product = $product;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listArray = $this->product->all();
        return view('admin.product.index', compact('listArray'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Subcategory $subcategory)
    {
        
        $categories = $subcategory->getActive();
        
        return view('admin.product.create', compact('categories'));    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request, ImageService $media, DocumentService $document)
    {
        $data = $request->all();
        $file = $request->file('file');
        if (in_array($file->guessExtension(), ['jpeg', 'png'])) {
            $data['filename'] = $media->upload($file, config('settings.image.product'));
        }
        $data['attachment'] = $document->upload($request->file('attachment'), config('settings.file.product'));
        $product = $this->product->create($data);
        return redirect(route('admin.product.edit', $product->id));
        \flash('Proizvod je uspešno kreiran')->success();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Subcategory $subcategory, $id)
    {
        $product = $this->product->find($id);
        $subcategories = $subcategory->getActive();
        return view('admin.product.edit', compact('product', 'subcategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, ImageService $media, DocumentService $document, $id)
    {
        $product = $this->product->find($id);
        $data = $request->all();

        $data['filename'] = $media->upload($request->file('file'), config('settings.image.product'));
        $data['attachment'] = $document->upload($request->file('attachment'), config('settings.file.product'));

        if ($data['filename']) {
            $media->delete($this->product->find($id)->filename, config('settings.image.product'));
        }

        if ($data['attachment']) {
            $media->delete($this->product->find($id)->filename, config('settings.file.product'));
        }

        $this->product->update($id, $data);

        \flash('Proizvod je uspešno izmenjen')->success();
        return redirect(route('admin.product.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->deleteItem($this->product, $id);
    }

    public function changeState(Request $request)
    {
        return $this->changeStatus($this->product, $request->id);
    }

    public function slugify(Request $request)
    {
        return ['slug' => $this->product->slugify($request->all())];
    }

    public function reorder(Request $request)
    {
        if ($request->get('sort_item')) {
            foreach ($request->get('sort_item') as $index => $itemId) {
                $this->product->setOrder($itemId, $index);
            }
            return ['status' => 'success'];
        }
    }
}
