<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Auth\AuthManager;


class AuthController extends Controller
{
    public function login()
    {
        return view('admin.auth.login');
    }

    public function postLogin(Request $request, AuthManager $auth)
    {
        if ($auth->attempt(['email' => $request->get('email'), 'password' => $request->get('password'), 'is_active' => 1])) {
            $auth->loginUsingId($auth->id());
            return redirect(route('admin.dashboard.index'));
        } else {
            return redirect(route('admin.auth.login'))->withInput($request->only('email', 'remember'))->withErrors(['email']);
        }
    }

    public function logout(AuthManager $auth)
    {
        $auth->logout();
        return redirect(route('admin.auth.login'));
    }
}
