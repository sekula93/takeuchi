<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\File\Image as ImageService;
use App\Repositories\Post\PostInterface;
use App\Repositories\PostCategory\PostCategoryInterface;
use App\Http\Requests\PostRequest;

class PostController extends BaseController
{
    protected $post;

    public function __construct(PostInterface $post)
    {
        $this->post = $post;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PostCategoryInterface $category)
    {
        $listArray = $this->post->all();
        $categories = $category->allActive();   
        return view('admin.post.index', compact('listArray', 'categories'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(PostCategoryInterface $category)
    {
        $categories = $category->allActive();
        return view('admin.post.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request, ImageService $media)
    {
        $data = $request->all();
        $file = $request->file('file');
        if (in_array($file->guessExtension(), ['jpeg', 'png'])) {
            $data['filename'] = $media->upload($file, config('settings.image.post'));
        }
        $this->post->create($data);
        return redirect(route('admin.post.index'));
        \flash('Post je uspešno kreiran')->success();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PostCategoryInterface $category, $id)
    {
        $post = $this->post->find($id);
        $categories = $category->allActive();
        // $created_at = date("d-M-Y", strtotime($post->created_at));
        return view('admin.post.edit', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request,ImageService $media, $id)
    {
        // try {

            $post = $this->post->find($id);
            $data = $request->all();

            $data['filename'] = $media->upload($request->file('file'), config('settings.image.post'));
            // $data['image'] = $image->upload($request->file('image'), config('settings.image.post.featured'));
            // $data['landscape_image'] = $image->upload($request->file('landscape_image'), config('settings.image.post.landscape'));
            if ($data['filename']) {
                $media->delete($this->post->find($id)->filename, config('settings.image.post'));
            }
            // if ($data['image']) {
            //     $image->delete($this->post->find($id)->image, config('settings.image.post.featured'));
            // }
            // if ($data['landscape_image']) {
            //     $image->delete($this->post->find($id)->landscape_image, config('settings.image.post.landscape'));
            // }
            $this->post->update($id, $data);

            \flash('Post je uspešno izmenjen')->success();
            return redirect(route('admin.post.edit', $id));
        // } catch (\Exception $e) {
            // dd($e);
            // \flash('Dogodila se greška prilikom izmene posta')->error();
        // }

        // return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->deleteItem($this->post, $id);
    }

    public function changeState(Request $request)
    {
        return $this->changeStatus($this->post, $request->id);
    }

    public function slugify(Request $request)
    {
        return ['slug' => $this->post->slugify($request->all())];
    }

    public function reorder(Request $request)
    {
        if ($request->get('sort_item')) {
            foreach ($request->get('sort_item') as $index => $itemId) {
                $this->post->setOrder($itemId, $index);
            }
            return ['status' => 'success'];
        }
    }
}
