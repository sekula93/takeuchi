<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\File\Image as ImageService;
use App\Repositories\Slide\SlideInterface;
use App\Http\Requests\SlideRequest;

class SliderController extends BaseController
{
    protected $slide;

    public function __construct(SlideInterface $slide)
    {
        $this->slide = $slide;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listArray = $this->slide->all();
        return view('admin.slider.index', compact('listArray'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SlideRequest $request, ImageService $media)
    {
        // dd($request->all());
        try {
            $data = $request->all();
            $file = $request->file('file');

            if (in_array($file->guessExtension(), ['jpeg', 'png'])) {
                $data['filename'] = $media->upload($file, config('settings.image.slide'));
            }
            $this->slide->create($data);
            \flash('Slajd je uspešno kreiran')->success();
            
            return redirect(route('admin.slider.index'));
        } catch (\Exception $e) {
            dd($e);
            \Notification::error('Dogodila se greška prilikom kreiranja slajda');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slide = $this->slide->find($id);
        return view('admin.slider.edit', compact('slide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SlideRequest $request, ImageService $media, $id)
    {
        $slide = $this->slide->find($id);
        $data = $request->all();

        $data['filename'] = $media->upload($request->file('file'), config('settings.image.slide'));
        // $data['image'] = $image->upload($request->file('image'), config('settings.image.slide.featured'));
        // $data['landscape_image'] = $image->upload($request->file('landscape_image'), config('settings.image.slide.landscape'));
        if ($data['filename']) {
            $media->delete($this->slide->find($id)->filename, config('settings.image.slide'));
        }
        // if ($data['image']) {
        //     $image->delete($this->slide->find($id)->image, config('settings.image.slide.featured'));
        // }
        // if ($data['landscape_image']) {
        //     $image->delete($this->slide->find($id)->landscape_image, config('settings.image.slide.landscape'));
        // }
        $this->slide->update($id, $data);

        \flash('Slide je uspešno izmenjen')->success();
        return redirect(route('admin.slider.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->deleteItem($this->slide, $id);
    }

    public function changeState(Request $request)
    {
        return $this->changeStatus($this->slide, $request->id);
    }

    public function reorder(Request $request)
    {
        try {
            $sortArray = $request->get('sort_item');
            foreach ($sortArray as $index => $id) {
                $this->slide->setOrder($id, $index);
            }
        } catch (\Exception $e) {
            dd($e);
        }
    }
}
