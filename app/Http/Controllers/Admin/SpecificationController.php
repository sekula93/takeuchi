<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Specification\SpecificationInterface;
use App\Repositories\Product\ProductInterface;

class SpecificationController extends BaseController
{
    
    protected $specification;

    public function __construct(SpecificationInterface $specification)
    {
        $this->specification = $specification;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listArray = $this->specification->all();
        return view('admin.specification.index', compact('listArray'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ProductInterface $product)
    {
        $products = $product->getActive();
        return view('admin.specification.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = $request->all();
        $this->specification->create($data);
        \flash('Specifikacija je uspešno kreirana')->success();
        return redirect(route('admin.specification.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductInterface $product, $id)
    {
        $products = $product->getActive();
        $specification = $this->specification->find($id);
        $relatedProducts = [];
        foreach($specification->relatedProducts as $proizvod) {
            array_push($relatedProducts, $proizvod->id);
        }
        // $relatedProducts = $specification->relatedProducts;
        return view('admin.specification.edit', compact('specification', 'products', 'relatedProducts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $specification = $this->specification->find($id);
        $data = $request->all();
        // dd($data);
        $this->specification->update($id, $data);
        \flash('Specifikacija je uspešno izmenjen')->success();
        return redirect(route('admin.specification.index'));
    }

    public function destroy($id)
    {
        return $this->deleteItem($this->specification, $id);
    }

    public function changeState(Request $request)
    {
        return $this->changeStatus($this->specification, $request->id);
    }

}
