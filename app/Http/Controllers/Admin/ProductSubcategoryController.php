<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ProductSubcategory\ProductSubcategoryInterface;
use App\Repositories\ProductCategory\ProductCategoryInterface;
use App\Http\Requests\ProductSubcategoryRequest;

class ProductSubcategoryController extends BaseController
{

    protected $subcategory;

    public function __construct(ProductSubcategoryInterface $subcategory)
    {
        $this->subcategory = $subcategory;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listArray = $this->subcategory->all();
        return view('admin.product.subcategory.index', compact('listArray'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ProductCategoryInterface $category)
    {
        $categories = $category->all();
        return view('admin.product.subcategory.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(ProductSubcategoryRequest $request)
    public function store(Request $request)
    {
        $data = $request->all();
        $this->subcategory->create($data);
        return redirect(route('admin.product-subcategory.index'));
        \flash('Podkategorija je uspešno kreirana')->success();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductCategoryInterface $category, $id)
    {
        $categories = $category->all();
        $subcategory = $this->subcategory->find($id);
        return view('admin.product.subcategory.edit', compact('subcategory', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductSubcategoryRequest $request, $id)
    {
        $data = $request->all();
        $this->subcategory->update($id, $data);
        \flash('Kategorija je uspešno izmenjena')->success();
        return redirect(route('admin.product-subcategory.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->deleteItem($this->subcategory, $id);
    }

    public function changeState(Request $request)
    {
        return $this->changeStatus($this->subcategory, $request->id);
    }

    public function slugify(Request $request)
    {
        return ['slug' => $this->subcategory->slugify($request->all())];
    }

    public function reorder(Request $request)
    {
        if ($request->get('sort_item')) {
            foreach ($request->get('sort_item') as $index => $itemId) {
                $this->subcategory->setOrder($itemId, $index);
            }
            return ['status' => 'success'];
        }
    }
}
