<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\PostCategory\PostCategoryInterface;

class PostCategoryController extends BaseController
{
    protected $category;

    public function __construct(PostCategoryInterface $category)
    {
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listArray = $this->category->all();
        return view('admin.post-category.index', compact('listArray'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.post-category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        // dd($data);
        $this->category->create($data);
        return redirect(route('admin.post-category.index'));
        \flash('Podkategorija je uspešno kreirana')->success();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->category->find($id);
        return view('admin.post-category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $this->category->update($id, $data);
        
        \flash('Kategorija je uspešno izmenjena')->success();
        return redirect(route('admin.post-category.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->deleteItem($this->category, $id);
    }

    public function changeState(Request $request)
    {
        return $this->changeStatus($this->category, $request->id);
    }

    public function slugify(Request $request)
    {
        return ['slug' => $this->category->slugify($request->all())];
    }

    public function reorder(Request $request)
    {
        if ($request->get('sort_item')) {
            foreach ($request->get('sort_item') as $index => $itemId) {
                $this->category->setOrder($itemId, $index);
            }
            return ['status' => 'success'];
        }
    }
}
