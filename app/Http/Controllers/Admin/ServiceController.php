<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Service\ServiceInterface;
use App\Service;


class ServiceController extends BaseController
{
    protected $service;

    public function __construct(ServiceInterface $service)
    {
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listArray = $this->service->all();
        return view('admin.service.index', compact('listArray'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->all();
            $this->service->create($data);
            return redirect(route('admin.service.index'));
            \flash('Usluga je uspešno kreirana')->success();
        } catch (Exception $e) {
            dd($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = $this->service->find($id);
        return view('admin.service.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->all();
            $this->service->update($id, $data);
            return redirect(route('admin.service.index'));
            \flash('Usluga je uspešno uredjena')->success();
        } catch (Exception $e) {
            dd($e);
        }
    }

    public function destroy($id)
    {
        return $this->deleteItem($this->service, $id);
    }

    public function changeState(Request $request)
    {
        return $this->changeStatus($this->service, $request->id);
    }

    public function slugify(Request $request)
    {
        return ['slug' => $this->service->slugify($request->all())];
    }

    public function reorder(Request $request)
    {
        if ($request->get('sort_item')) {
            foreach ($request->get('sort_item') as $index => $itemId) {
                $this->service->setOrder($itemId, $index);
            }
            return ['status' => 'success'];
        }
    }
}
