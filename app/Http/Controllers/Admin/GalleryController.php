<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Gallery\GalleryInterface;

class GalleryController extends BaseController
{

    protected $gallery;

    public function __construct(GalleryInterface $gallery)
    {
        $this->gallery = $gallery;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        // $file = $request->file('file');
        // if (in_array($file->guessExtension(), ['jpeg', 'png'])) {
        //     $data['filename'] = $media->upload($file, config('settings.image.gallery'));
        // }
        $gallery = $this->gallery->create($data);
        return redirect(route('admin.gallery.edit', $gallery->id));
        \flash('Galerija je uspešno kreirana')->success();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gallery = $this->gallery->find($id);
        return view('admin.gallery.edit', compact('gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gallery = $this->gallery->find($id);
        $data = $request->all();

        // $data['filename'] = $media->upload($request->file('file'), config('settings.image.gallery'));
        // if ($data['filename']) {
            // $media->delete($this->gallery->find($id)->filename, config('settings.image.gallery'));
        // }
        $this->gallery->update($id, $data);

        \flash('Galerija je uspešno izmenjen')->success();
        return redirect(route('admin.gallery.edit', $gallery->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->deleteItem($this->gallery, $id);
    }

    public function changeState(Request $request)
    {
        return $this->changeStatus($this->gallery, $request->id);
    }

    public function slugify(Request $request)
    {
        return ['slug' => $this->gallery->slugify($request->all())];
    }

    public function reorder(Request $request)
    {
        if ($request->get('sort_item')) {
            foreach ($request->get('sort_item') as $index => $itemId) {
                $this->gallery->setOrder($itemId, $index);
            }
            return ['status' => 'success'];
        }
    }
}
