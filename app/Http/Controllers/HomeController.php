<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Slide\SlideInterface;
use App\Repositories\Associate\AssociateInterface;
use App\Repositories\ProductCategory\ProductCategoryInterface;
use App\Repositories\ProductSubcategory\ProductSubcategoryInterface;
use App\Repositories\Product\ProductInterface;
use App\Repositories\Gallery\GalleryInterface;
use App\Repositories\Post\PostInterface;
use App\Repositories\Service\ServiceInterface;
use Mail;


class HomeController extends Controller
{
    public function index(SlideInterface $slide, AssociateInterface $associate, PostInterface $post, ServiceInterface $service)
    {   
        $slides = $slide->getForFront();
        $associates = $associate->getForFront();
        $recentPosts = $post->getRecentPosts();
        $services = $service->getForFront();
        return view('front.home.index', compact('slides', 'associates', 'recentPosts', 'services'));
    }

    public function company(GalleryInterface $gallery, PostInterface $company)
    {
        $pictures = $gallery->allActive();
        $companies = $company->getAboutUs();
        return view('front.company.index', compact('companies', 'pictures'));
    }
    
    public function showCompany(PostInterface $company, $slug)
    {
        $post = $company->findBySlug($slug);
        
        return view('front.post.show', compact('post'));
    }
    
    public function products(ProductCategoryInterface $category)
    {
        $categories = $category->getActive();
        
        return view('front.product.index', compact('categories'));
    }

    public function productList(ProductSubcategoryInterface $subcategory, ProductInterface $product, $cat)
    {
        $subcat = $subcategory->getSubcategoryBySlug($cat);
        $products = $product->getBySubcategory($subcat->id, 1, 8);
        return view('front.product.list', compact('subcat', 'products'));
    }

    public function productShow(ProductInterface $product, ProductSubcategoryInterface $subcategory, $cat, $slug)
    {
        $item = $product->findBySlug($slug);
        $subcat = $subcategory->getSubcategoryBySlug($cat);
        return view('front.product.show', compact('item', 'subcat'));
    }

    public function gallery(GalleryInterface $gallery)
    {
        $pictures = $gallery->allActive();
        return view('front.gallery.index', compact('pictures'));
    }

    public function rental(ProductInterface $product)
    {
        $products = $product->getBySubcategory(null, 2, 8);
        return view('front.rental.list', compact('products'));
    }

    public function showRental(ProductInterface $product, $slug)
    {
        $item = $product->findBySlug($slug);
        return view('front.rental.show', compact('item'));
    }

    public function used(ProductInterface $product)
    {
        $products = $product->getBySubcategory(null, 3, 8);
        return view('front.used.list', compact('products'));
    }

    public function showUsed(ProductInterface $product, $slug)
    {
        $item = $product->findBySlug($slug);
        return view('front.used.show', compact('item'));
    }

    public function showService(PostInterface $company)
    {
        $post = $company->find(3);
        return view('front.post.show', compact('post'));
    }
    
    public function post(PostInterface $post)
    {
        $posts = $post->getForFront();
        return view('front.post.index', compact('posts'));
    }

    public function showPost(PostInterface $item, $slug)
    {
        $post = $item->findBySlug($slug);
        return view('front.post.show', compact('post'));
    }

    public function contact()
    {
        return view('front.contact.index');
    }

    public function career()
    {
        return view('front.career.index');
    }

    public function contactUs(Request $request)
    {
        $data = $request->all();
        // $filename =  $data['attachment']->getClientOriginalName();
        if (isset($data['attachment'])) {
            $file = $data['attachment'];
            $file->move("uploads/cv", $data['attachment']->getClientOriginalName());
            Mail::send('front.email.send', ['name' => $data['name'], 'email' => $data['email'], 'content' => $data['text'], 'phone' => $data['phone']], function ($message) use ($data)
            {
    
                $message->from('office@takeuchi-srbija.com', $data['name']);
    
                $message->to(['nikolasekulic93@gmail.com', 'info@takeuchi-srbija.com'])->subject('Zaposlenje')->replyTo($data['email'])->attach('uploads/cv/'.$data['attachment']->getClientOriginalName());

            });
        } else if (isset($data['daterange'])) {
            Mail::send('front.email.send', [
                'name' => $data['name'], 
                'email' => $data['email'], 
                'content' => $data['text'], 
                'phone' => $data['phone'],
                'daterange' => $data['daterange'],
                'model' => $data['model']], function ($message) use ($data)
            {
    
                $message->from('office@takeuchi-srbija.com', $data['name']);
    
                $message->to(['nikolasekulic93@gmail.com', 'info@takeuchi-srbija.com'])->subject('Poruka sa sajta')->replyTo($data['email']);
    
            });
        } else {
            Mail::send('front.email.send', ['name' => $data['name'], 'email' => $data['email'], 'content' => $data['text'], 'phone' => $data['phone']], function ($message) use ($data)
            {
    
                $message->from('office@takeuchi-srbija.com', $data['name']);
    
                $message->to(['nikolasekulic93@gmail.com', 'info@takeuchi-srbija.com'])->subject('Poruka sa sajta')->replyTo($data['email']);
    
            });
        }



        // $content = $request->input('text');
        // var_dump($request->all()); die();

        return response()->json(['status' => 'success']);
    }

    
}
