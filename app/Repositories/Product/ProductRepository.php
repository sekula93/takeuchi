<?php

namespace App\Repositories\Product;

use App\Repositories\Traits\StatusTrait;
use App\Product;

class ProductRepository implements ProductInterface
{
    use StatusTrait;

    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function all()
    {
        return $this->product->orderBy("order")->get();
    }

    public function find($id)
    {
        return $this->product->find($id);
    }

    public function findBySlug($slug)
    {
        return $this->product->where('slug', $slug)->first();
    }

    public function getActive()
    {
        return $this->product->where('is_active', 1)->orderBy('title')->get();
    }

    public function getBySubcategory($subcategory = null, $type, $paginate)
    {
        if ($subcategory) {
            return $this->product->where('product_subcategory_id', $subcategory)->where('type', $type)->paginate($paginate);
        } else {
            return $this->product->where('type', $type)->paginate($paginate);
        }
    }

    public function create($input)
    {
        if (!$input['text']) {
            $input['text'] = NULL;
        }

        \DB::beginTransaction();
        $product = $this->product->create([
            'title'      => $input['title'],
            'text'       => $input['text'],
            'slug'       => $input['slug'],
            'type'       => $input['type'],
            'product_subcategory_id' => $input['subcategory_id'],
            'is_active'  => $input['is_active'],
            'filename'   => $input['filename'],
            'attachment'   => $input['attachment'],
            'attachment_label'   => $input['attachment_label']
        ]);
        \DB::commit();

        return $product;
    }

    public function update($id, $input)
    {
        $product = $this->find($id);
        
        if (!$input['filename']) {
            $input['filename'] = $product->filename;
        }
        if (!$input['attachment']) {
            $input['attachment'] = $product->filename;
        }
        if (!$input['text']) {
            $input['text'] = NULL;
        }

        \DB::beginTransaction();
        $product->update([
            'title'      => $input['title'],
            'text'       => $input['text'],
            'slug'       => $input['slug'],
            'type'       => $input['type'],
            'product_subcategory_id' => $input['subcategory_id'],            
            'is_active'  => $input['is_active'],
            'filename'   => $input['filename'],
            'attachment'   => $input['attachment'],
            'attachment_label'   => $input['attachment_label']
        ]);
        \DB::commit();
    }

    public function slugify($input)
    {
        if (isset($input['id']) && $input['id'] != 0) {
            $product = $this->product->find($input['id']);
            if ($product->slug == str_slug($input['title'])) {
                $slug = $product->slug;
            } else {
                $slug = $this->_generateSlug($input['title']);
            }
        } else {
            $slug = $this->_generateSlug($input['title']);
        }

        return $slug;
    }

    public function setOrder($id, $index)
    {
        $product = $this->product->find($id);
        $product->update(['order' => $index]);
    }

    public function delete($id)
    {
        $slide = $this->find($id);
        if (!$slide) {
            return false;
        }

        return $slide->delete();
    }

    private function _generateSlug($title)
    {
        $i = 1;
        $notUnique = true;
        $forSlug = $title;
        do {
            $slug = str_slug($forSlug);
            if ($this->product->where('slug', str_slug($slug))->count()) {
                $forSlug = $title.'-'.$i;
            } else {
                $notUnique = false;
            }
            $i++;
        } while ($notUnique);

        return $slug;
    }


}