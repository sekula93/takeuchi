<?php

namespace App\Repositories\Slide;

use App\Repositories\Traits\StatusTrait;
use App\Slide;

class SlideRepository implements SlideInterface
{
    use StatusTrait;

    protected $slide;

    public function __construct(Slide $slide)
    {
        $this->slide = $slide;
    }

    public function all()
    {
        return $this->slide->orderBy("order")->get();
    }

    public function getForFront()
    {
        return $this->slide->where('is_active', 1)->orderBy('order')->get();
    }

    private function _makeArrayItem($slide)
    {
        $translation = $slide->translations->where('code', \App::getLocale())->first();
        
        return [
            'type' => $slide->type == 1 ? 'image' : 'video',
            'filename' => $slide->filename,
            'fallback_image' => $slide->fallback_image,
            'title' => $translation->pivot->title,
            'subtitle' => $translation->pivot->subtitle,
            // 'button_label' => $translation->pivot->button_label,
            // 'text_color_class' => $textColorClass,
            // 'url_type' => $slide->url_type,
            'url' => $translation->pivot->url,
            'order' =>$slide->order
        ];
    }

    public function find($id)
    {
        return $this->slide->find($id);
    }

    public function create($input)
    {
        \DB::beginTransaction();
        $slide = $this->slide->create([
            'title'  => $input['title'],
            'subtitle'      => $input['subtitle'],
            'link'      => $input['link'],
            'is_active'    => $input['is_active'],
            'filename'  => $input['filename']
        ]);
        \DB::commit();
    }

    public function update($id, $input)
    {
        $slide = $this->find($id);
        if (!isset($input['filename']) || $input['filename'] === false) {
            $input['filename'] = $slide->filename;
        }


        \DB::beginTransaction();
        $slide->update([
            'title'  => $input['title'],
            'subtitle'      => $input['subtitle'],
            'link'      => $input['link'],
            'is_active'    => $input['is_active'],
            'filename'  => $input['filename']
        ]);
        \DB::commit();
    }

    public function paginate()
    {
        return $this->slide->orderBy('created_at', 'DESC')->paginate(config('settings.pagination.items', 15));
    }

    public function delete($id)
    {
        $slide = $this->find($id);
        if (!$slide) {
            return false;
        }

        return $slide->delete();
    }

    public function setOrder($id, $index)
    {
        $slide = $this->find($id);
        $slide->update(['order' => $index]);
    }
}