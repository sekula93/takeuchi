<?php

namespace App\Repositories\Associate;

use App\Repositories\Traits\StatusTrait;
use App\Associate;

class AssociateRepository implements AssociateInterface
{
    use StatusTrait;

    protected $associate;

    public function __construct(Associate $associate)
    {
        $this->associate = $associate;
    }

    public function all()
    {
        return $this->associate->orderBy("order")->get();
    }

    public function find($id)
    {
        return $this->associate->find($id);
    }

    public function getForFront()
    {
        return $this->associate->where('is_active', 1)->orderBy('order')->get();
    }

    public function create($input)
    {
        \DB::beginTransaction();
        $associate = $this->associate->create([
            'name'      => $input['name'],
            'is_active'  => $input['is_active'],
            'filename'   => $input['filename']
        ]);
        \DB::commit();
    }

    public function update($id, $input)
    {
        $associate = $this->find($id);
        
        if (!$input['filename']) {
            $input['filename'] = $associate->filename;
        }

        \DB::beginTransaction();
        $associate->update([
            'name'      => $input['name'],
            'is_active'  => $input['is_active'],
            'filename'   => $input['filename']
        ]);
        \DB::commit();
    }

    public function slugify($input)
    {
        if (isset($input['id']) && $input['id'] != 0) {
            $associate = $this->associate->find($input['id']);
            if ($associate->slug == str_slug($input['title'])) {
                $slug = $associate->slug;
            } else {
                $slug = $this->_generateSlug($input['title']);
            }
        } else {
            $slug = $this->_generateSlug($input['title']);
        }

        return $slug;
    }

    public function setOrder($id, $index)
    {
        $associate = $this->associate->find($id);
        $associate->update(['order' => $index]);
    }

    public function delete($id)
    {
        $slide = $this->find($id);
        if (!$slide) {
            return false;
        }

        return $slide->delete();
    }

    private function _generateSlug($title)
    {
        $i = 1;
        $notUnique = true;
        $forSlug = $title;
        do {
            $slug = str_slug($forSlug);
            if ($this->associate->where('slug', str_slug($slug))->count()) {
                $forSlug = $title.'-'.$i;
            } else {
                $notUnique = false;
            }
            $i++;
        } while ($notUnique);

        return $slug;
    }


}