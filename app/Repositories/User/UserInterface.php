<?php

namespace App\Repositories\User;

interface UserInterface {

    public function all();

    public function count($role);

    public function find($id);

    // public function findByEmail($email);

    public function findByRole($role);

    public function create(Type\User $user);

    public function update($id, Type\User $user);

    public function delete($id);

    // public function paginateRole($role);

    // public function setPassword($id, $password);

    public function changeStatus($id);

    // public function loggedInUpdate($id);

}