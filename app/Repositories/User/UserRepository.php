<?php

namespace App\Repositories\User;

use App\MemberProfile;
use App\Repositories\Traits\StatusTrait;
use App\User;
use App\UserToken;
use Illuminate\Support\Facades\DB;

class UserRepository implements UserInterface
{
    use StatusTrait;

    protected $user;
    // protected $memberProfile;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function all()
    {
        return $this->user->all();
    }

    public function findByRole($role)
    {
        return $this->user->where('role', $role)->get();
    }

    public function count($role)
    {
        return $this->user->where('role', $role)->count();
    }

    public function find($id)
    {
        return $this->user->findOrFail($id);
    }

    public function create(Type\User $userType)
    {
        \DB::beginTransaction();

        $userTypeData = $userType->prepareData();
        $user = $this->user->create($userTypeData);

        if ($userType->userRelation) {
            $user->{$userType->userRelation}()->create($userType->prepareLinkedData());
        }

        $user->plainPassword = isset($userTypeData['password']) ? $userTypeData['password'] : null;

        \DB::commit();

        return $user;
    }

    public function update($id, Type\User $userType)
    {
        \DB::beginTransaction();

        $user = $this->find($id);
        $user->update($userType->prepareData());

        if ($userType->userRelation) {
            $user->{$userType->userRelation}()->update($userType->prepareLinkedData());
        }

        \DB::commit();
        return true;
    }

    public function delete($id)
    {
        $user = $this->find($id);
        return $user->delete();
    }

}
