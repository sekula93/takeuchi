<?php

namespace App\Repositories\Service;

use App\Repositories\Traits\StatusTrait;
use App\Service;

class ServiceRepository implements ServiceInterface
{
    use StatusTrait;

    protected $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    public function all()
    {
        return $this->service->orderBy("order")->get();
    }

    public function find($id)
    {
        return $this->service->find($id);
    }

    public function getForFront()
    {
        return $this->service->where('is_active', 1)->orderBy('order')->limit(3)->get();
    }

    public function create($input)
    {
        \DB::beginTransaction();
        $service = $this->service->create([
            'title'      => $input['title'],
            'subtitle'      => $input['subtitle'],
            'is_active'  => $input['is_active'],
            // 'filename'   => $input['filename']
        ]);
        \DB::commit();
    }

    public function update($id, $input)
    {
        $service = $this->find($id);
        
        // if (!$input['filename']) {
        //     $input['filename'] = $service->filename;
        // }

        \DB::beginTransaction();
        $service->update([
            'title'      => $input['title'],
            'subtitle'      => $input['subtitle'],
            'is_active'  => $input['is_active'],
            // 'filename'   => $input['filename']
        ]);
        \DB::commit();
    }

    public function slugify($input)
    {
        if (isset($input['id']) && $input['id'] != 0) {
            $service = $this->service->find($input['id']);
            if ($service->slug == str_slug($input['title'])) {
                $slug = $service->slug;
            } else {
                $slug = $this->_generateSlug($input['title']);
            }
        } else {
            $slug = $this->_generateSlug($input['title']);
        }

        return $slug;
    }

    public function setOrder($id, $index)
    {
        $service = $this->service->find($id);
        $service->update(['order' => $index]);
    }

    public function delete($id)
    {
        $slide = $this->find($id);
        if (!$slide) {
            return false;
        }

        return $slide->delete();
    }

    private function _generateSlug($title)
    {
        $i = 1;
        $notUnique = true;
        $forSlug = $title;
        do {
            $slug = str_slug($forSlug);
            if ($this->service->where('slug', str_slug($slug))->count()) {
                $forSlug = $title.'-'.$i;
            } else {
                $notUnique = false;
            }
            $i++;
        } while ($notUnique);

        return $slug;
    }


}