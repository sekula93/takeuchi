<?php

namespace App\Repositories\Gallery;

use App\Repositories\Traits\StatusTrait;
use App\Gallery;

class GalleryRepository implements GalleryInterface
{
    use StatusTrait;

    protected $gallery;

    public function __construct(Gallery $gallery)
    {
        $this->gallery = $gallery;
    }

    public function all()
    {
        return $this->gallery->with('category')->orderBy("order")->get();
    }

    public function find($id)
    {
        return $this->gallery->find($id);
    }

    public function allActive()
    {
        return $this->gallery->where('is_active', 1)->get();
    }

    public function getRecentPosts()
    {
        return $this->gallery->where('is_active', 1)->orderBy('order')->get();
    }

    public function create($input)
    {
        \DB::beginTransaction();
        $gallery = $this->gallery->create([
            'title'      => $input['title'],
            'slug'       => $input['slug'],
            'excerpt'    => $input['excerpt'],
            'is_active'  => $input['is_active'],
            // 'filename'   => $input['filename'],
        ]);
        \DB::commit();

        return $gallery;
    }

    public function update($id, $input)
    {
        $gallery = $this->find($id);
        
        // if (!$input['filename']) {
        //     $input['filename'] = $gallery->filename;
        // }

        \DB::beginTransaction();
        $gallery->update([
            'title'      => $input['title'],
            'slug'       => $input['slug'],
            'excerpt'    => $input['excerpt'],
            'is_active'  => $input['is_active'],
            // 'filename'   => $input['filename'],
        ]);
        \DB::commit();
    }

    public function slugify($input)
    {
        if (isset($input['id']) && $input['id'] != 0) {
            $gallery = $this->gallery->find($input['id']);
            if ($gallery->slug == str_slug($input['title'])) {
                $slug = $gallery->slug;
            } else {
                $slug = $this->_generateSlug($input['title']);
            }
        } else {
            $slug = $this->_generateSlug($input['title']);
        }

        return $slug;
    }

    public function setOrder($id, $index)
    {
        $gallery = $this->gallery->find($id);
        $gallery->update(['order' => $index]);
    }

    public function delete($id)
    {
        $slide = $this->find($id);
        if (!$slide) {
            return false;
        }

        return $slide->delete();
    }

    private function _generateSlug($title)
    {
        $i = 1;
        $notUnique = true;
        $forSlug = $title;
        do {
            $slug = str_slug($forSlug);
            if ($this->gallery->where('slug', str_slug($slug))->count()) {
                $forSlug = $title.'-'.$i;
            } else {
                $notUnique = false;
            }
            $i++;
        } while ($notUnique);

        return $slug;
    }


}