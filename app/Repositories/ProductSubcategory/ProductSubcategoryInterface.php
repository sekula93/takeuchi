<?php

namespace App\Repositories\ProductSubcategory;

interface ProductSubcategoryInterface
{

    public function all();

    public function create($input);

    public function update($id, $input);
    
    public function slugify($input);

    public function setOrder($id, $index);

    public function delete($id);

}