<?php

namespace App\Repositories\Specification;

interface SpecificationInterface
{

    public function all();

    public function create($input);

    public function update($id, $input);
    
    public function setOrder($id, $index);

    public function delete($id);

}