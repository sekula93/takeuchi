<?php

namespace App\Repositories\Specification;

use App\Repositories\Traits\StatusTrait;
use App\Specification;

class SpecificationRepository implements SpecificationInterface
{
    use StatusTrait;

    protected $specification;

    public function __construct(Specification $specification)
    {
        $this->specification = $specification;
    }

    public function all()
    {
        return $this->specification->orderBy("name")->orderBy('product_id')->get();
    }

    public function find($id)
    {
        return $this->specification->find($id);
    }

    public function create($input)
    {
        \DB::beginTransaction();
        $specification = $this->specification->create([
            'name'      => $input['name'],
            'text'       => $input['text'],
            'product_id' => $input['product_id'],
            'is_active'  => $input['is_active'],
        ]);

        if (isset($input['related_products'])) {
            $specification->relatedProducts()->attach($input['related_products']);
        }
        \DB::commit();
    }

    public function update($id, $input)
    {
        $specification = $this->find($id);
        
        \DB::beginTransaction();
        $specification->update([
            'name'      => $input['name'],
            'text'       => $input['text'],
            'product_id' => $input['product_id'],            
            'is_active'  => $input['is_active'],
        ]);

        if (isset($input['related_products'])) {
            $specification->relatedProducts()->sync($input['related_products']);
        }

        \DB::commit();
    }


    public function setOrder($id, $index)
    {
        $specification = $this->specification->find($id);
        $specification->update(['order' => $index]);
    }

    public function delete($id)
    {
        $slide = $this->find($id);
        if (!$slide) {
            return false;
        }

        return $slide->delete();
    }
}
