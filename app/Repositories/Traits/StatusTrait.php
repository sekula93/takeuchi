<?php

namespace App\Repositories\Traits;

trait StatusTrait
{

    public function changeStatus($id)
    {
        $object = $this->find($id);
        if ($object->is_active == 1) {
            $updatedStatus = 0;
        } else {
            $updatedStatus = 1;
        }
        
        if ($object->update(['is_active' => $updatedStatus])) {
            return $updatedStatus;
        } else {
            return false;
        }
    }

    public function deleteItem($id, $model)
    {
        if ($model->delete($id)) {
            $response = ['status' => 'success'];
        } else {
            $response = ['status' => 'error'];
        }

        return $response;
    }
}