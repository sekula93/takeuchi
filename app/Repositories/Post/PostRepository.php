<?php

namespace App\Repositories\Post;

use App\Repositories\Traits\StatusTrait;
use App\Post;

class PostRepository implements PostInterface
{
    use StatusTrait;

    protected $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function all()
    {
        return $this->post->with('category')->where('flag', 1)->orderBy("order")->get();
    }

    public function find($id)
    {
        return $this->post->find($id);
    }

    public function findBySlug($slug)   
    {
        return $this->post->where('slug', $slug)->first();
    }

    public function getRecentPosts()
    {
        return $this->post->where('is_active', 1)->where('flag', 1)->orderBy('order')->get();
    }

    public function getForFront()
    {
        return $this->post->with('category')->where('flag', 1)->orderBy("order")->paginate(9); 
    }

    // front company index
    public function getAboutUs()
    {
        return $this->post->orderBy('id')->limit(2)->get();
    }

    public function create($input)
    {
        if(!isset($input["excerpt"])) {
            $input['excerpt'] = '';
        }
        if(!isset($input["post_category_id"])) {
            $input['post_category_id'] = 1;
        }
        if(!isset($input["filename"])) {
            $input['filename'] = 0;
        }

        \DB::beginTransaction();
        $post = $this->post->create([
            'title'      => $input['title'],
            'text'       => $input['text'],
            'slug'       => $input['slug'],
            'post_category_id' => $input['post_category_id'],
            'excerpt'    => $input['excerpt'],
            'is_active'  => $input['is_active'],
            'filename'   => $input['filename'],
            'created_at' => $input['post_date']
        ]);
        \DB::commit();
    }

    public function update($id, $input)
    {
        $post = $this->find($id);
        
        if (!$input['filename']) {
            $input['filename'] = $post->filename;
        }

        if(!isset($input["excerpt"])) {
            $input['excerpt'] = '';
        }
        if(!isset($input["post_category_id"])) {
            $input['post_category_id'] = 1;
        }
        if(!isset($input["filename"])) {
            $input['filename'] = 0;
        }

        \DB::beginTransaction();
        $post->update([
            'title'      => $input['title'],
            'text'       => $input['text'],
            'slug'       => $input['slug'],
            'post_category_id' => $input['post_category_id'],            
            'excerpt'    => $input['excerpt'],
            'is_active'  => $input['is_active'],
            'filename'   => $input['filename'],
            'created_at' => $input['post_date']
        ]);
        \DB::commit();
    }

    public function slugify($input)
    {
        if (isset($input['id']) && $input['id'] != 0) {
            $post = $this->post->find($input['id']);
            if ($post->slug == str_slug($input['title'])) {
                $slug = $post->slug;
            } else {
                $slug = $this->_generateSlug($input['title']);
            }
        } else {
            $slug = $this->_generateSlug($input['title']);
        }

        return $slug;
    }

    public function setOrder($id, $index)
    {
        $post = $this->post->find($id);
        $post->update(['order' => $index]);
    }

    public function delete($id)
    {
        $slide = $this->find($id);
        if (!$slide) {
            return false;
        }

        return $slide->delete();
    }

    private function _generateSlug($title)
    {
        $i = 1;
        $notUnique = true;
        $forSlug = $title;
        do {
            $slug = str_slug($forSlug);
            if ($this->post->where('slug', str_slug($slug))->count()) {
                $forSlug = $title.'-'.$i;
            } else {
                $notUnique = false;
            }
            $i++;
        } while ($notUnique);

        return $slug;
    }


}