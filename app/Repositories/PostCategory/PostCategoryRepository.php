<?php

namespace App\Repositories\PostCategory;

use App\Repositories\Traits\StatusTrait;
use App\PostCategory;

class PostCategoryRepository implements PostCategoryInterface
{
    use StatusTrait;

    protected $category;

    public function __construct(PostCategory $category)
    {
        $this->category = $category;
    }

    public function all()
    {
        return $this->category->orderBy("order")->get();
    }

    public function allActive()
    {
        return $this->category->where('is_active', 1)->orderBy("order")->get();
    }

    public function find($id)
    {
        return $this->category->find($id);
    }

    public function create($input)
    {
        \DB::beginTransaction();
        $category = $this->category->create([
            'name'      => $input['name'],
            'slug'       => $input['slug'],
            // 'excerpt'    => $input['excerpt'],
            'is_active'  => $input['is_active'],
            // 'filename'   => $input['filename'],
        ]);
        \DB::commit();
    }

    public function update($id, $input)
    {
        $category = $this->find($id);

        \DB::beginTransaction();
        $category->update([
            'name'      => $input['name'],
            'slug'       => $input['slug'],
            // 'excerpt'    => $input['excerpt'],
            'is_active'  => $input['is_active'],
            // 'filename'   => $input['filename'],
        ]);
        \DB::commit();
    }

    public function slugify($input)
    {
        if (isset($input['id']) && $input['id'] != 0) {
            $category = $this->category->find($input['id']);
            if ($category->slug == str_slug($input['title'])) {
                $slug = $category->slug;
            } else {
                $slug = $this->_generateSlug($input['title']);
            }
        } else {
            $slug = $this->_generateSlug($input['title']);
        }

        return $slug;
    }

    public function setOrder($id, $index)
    {
        $category = $this->category->find($id);
        $category->update(['order' => $index]);
    }

    public function delete($id)
    {
        $slide = $this->find($id);
        if (!$slide) {
            return false;
        }

        return $slide->delete();
    }

    private function _generateSlug($title)
    {
        $i = 1;
        $notUnique = true;
        $forSlug = $title;
        do {
            $slug = str_slug($forSlug);
            if ($this->category->where('slug', str_slug($slug))->count()) {
                $forSlug = $title.'-'.$i;
            } else {
                $notUnique = false;
            }
            $i++;
        } while ($notUnique);

        return $slug;
    }

}