<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    protected $fillable = ['title', 'post_category_id', 'slug', 'text', 'filename', 'excerpt', 'order', 'is_active', 'document_label', 'document_filename', 'created_at'];

    public $timestamps = true;

    public function photos()
    {
        return $this->hasMany('App\PostPhoto');
    }

    public function category()
    {
        return $this->belongsTo('App\PostCategory', 'post_category_id');
    }
}
