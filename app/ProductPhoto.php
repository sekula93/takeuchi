<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPhoto extends Model
{
    protected $table = 'product_photos';

    protected $fillable = ['product_id', 'filename', 'is_active', 'order'];

    public $timestamps = false;

    public function post()
    {
        return $this->belongsTo('App\Product');
    }
}
