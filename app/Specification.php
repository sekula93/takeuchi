<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specification extends Model
{
    protected $table = 'specifications';

    protected $fillable = ['name', 'product_id', 'text', 'is_active',];

    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function relatedProducts()
    {
        return $this->belongsToMany('App\Product');
    }
}
