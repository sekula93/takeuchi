<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $table = 'slides';

    protected $fillable = ['title', 'subtitle', 'link', 'filename', 'is_active', 'order'];

    public $timestamps = false;
}
