<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'services';

    protected $fillable = ['title', 'subtitle', 'is_active', 'order'];

    public $timestamps = false;
}
