<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'product_categories';

    protected $fillable = ['name', 'slug','filename', 'order', 'is_active'];

    public $timestamps = false;

    public function productSubcategories()
    {
        return $this->hasMany('App\ProductSubcategory');
    }

    public function products()
    {
        return $this->hasManyThrough('App\Product', 'App\ProductSubcategory');
    }
}
