<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\User\UserInterface', 'App\Repositories\User\UserRepository');
        $this->app->bind('App\Repositories\Slide\SlideInterface', 'App\Repositories\Slide\SlideRepository');
        $this->app->bind('App\Repositories\Post\PostInterface', 'App\Repositories\Post\PostRepository');
        $this->app->bind('App\Repositories\PostPhoto\PostPhotoInterface', 'App\Repositories\PostPhoto\PostPhotoRepository');
        $this->app->bind('App\Repositories\ProductCategory\ProductCategoryInterface', 'App\Repositories\ProductCategory\ProductCategoryRepository');
        $this->app->bind('App\Repositories\ProductSubcategory\ProductSubcategoryInterface', 'App\Repositories\ProductSubcategory\ProductSubcategoryRepository');
        $this->app->bind('App\Repositories\PostCategory\PostCategoryInterface', 'App\Repositories\PostCategory\PostCategoryRepository');
        $this->app->bind('App\Repositories\Associate\AssociateInterface', 'App\Repositories\Associate\AssociateRepository');
        $this->app->bind('App\Repositories\Product\ProductInterface', 'App\Repositories\Product\ProductRepository');
        $this->app->bind('App\Repositories\ProductPhoto\ProductPhotoInterface', 'App\Repositories\ProductPhoto\ProductPhotoRepository');
        $this->app->bind('App\Repositories\Specification\SpecificationInterface', 'App\Repositories\Specification\SpecificationRepository');
        $this->app->bind('App\Repositories\Gallery\GalleryInterface', 'App\Repositories\Gallery\GalleryRepository');
        $this->app->bind('App\Repositories\GalleryPhoto\GalleryPhotoInterface', 'App\Repositories\GalleryPhoto\GalleryPhotoRepository');
        $this->app->bind('App\Repositories\Service\ServiceInterface', 'App\Repositories\Service\ServiceRepository');
	}
}

