<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSubcategory extends Model
{
    protected $table = 'product_subcategories';

    protected $fillable = ['name', 'product_category_id', 'slug', 'order', 'flag', 'is_active'];

    public $timestamps = false;

    public function productCategory()
    {
        return $this->belongsTo('App\ProductCategory');
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }

}
