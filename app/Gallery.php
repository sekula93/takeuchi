<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'galleries';

    protected $fillable = ['title', 'slug', 'excerpt', 'order', 'is_active'];

    public $timestamps = false;

    public function photos()
    {
        return $this->hasMany('App\GalleryPhoto');
    }
}
