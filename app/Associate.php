<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Associate extends Model
{
    protected $table = 'associates';

    protected $fillable = ['name', 'filename', 'is_active', 'order'];

    public $timestamps = false;
}
