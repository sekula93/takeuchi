<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = ['title', 'product_subcategory_id', 'type', 'slug', 'text', 'filename', 'attachment', 'order', 'is_active', 'attachment_label', 'created_at'];

    public $timestamps = false;

    // public function photos()
    // {
    //     return $this->hasMany(Photo::class);
    // }
    
    public function productSubcategory()
    {
        return $this->belongsTo('App\ProductSubcategory');
    }

    public function photos()
    {
        return $this->hasMany('App\ProductPhoto');
    }

    public function specifications()
    {
        return $this->hasMany('App\Specification');
    }

    public function relatedSpecifications()
    {
        return $this->belongsToMany('App\Specification');
    } 

}
