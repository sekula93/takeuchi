<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GalleryPhoto extends Model
{
    protected $table = 'gallery_photos';

    protected $fillable = ['gallery_id', 'filename', 'is_active', 'order'];

    public $timestamps = false;

    public function gallery()
    {
        return $this->belongsTo('App\Gallery');
    }
}
