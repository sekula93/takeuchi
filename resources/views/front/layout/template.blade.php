<?php $routeName = Route::currentRouteName(); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="/assets/front/dist/images/favicon.png" type="image/x-icon"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Takeuchi-Srbija</title>
    <meta name="description" content="Prodaja, servis i iznajmljivanje gradjevinskih masina, bagera, mini bagera, gusenicara, tockasa. Prodaja gradjevinske opreme i mehanizacije.">
    <meta name="keywords" content="Bageri, mini bageri, mini bageri Novi Sad, mini bageri Beograd, utovarivaci, gusenicari, bageri tockasi, kompaktni bageri, bageri Beograd, bageri Novi Sad">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,400i,500,700" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">    
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css'/>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/lightgallery@1.6.5/dist/css/lightgallery.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

    @if ($routeName == 'front.product.show')
        <meta property="og:url"                content="{{ url()->current() }}" />
        <meta property="og:type"               content="article" />
        <meta property="og:title"              content="{{ $item->title }}" />
        <meta property="og:description"        content="{{ $item->text }}" />
        <meta property="og:image"              content="{{ url("") }}{{config('settings.image.product.upload_dir').$item->filename }}" />
        <meta property="fb:app_id"             content="1689404694439169" />
    @endif

    @if ($routeName == 'front.post.show')
        <meta property="og:url"                content="{{ url()->current() }}" />
        <meta property="og:type"               content="article" />
        <meta property="og:title"              content="{{ $post->title }}" />
        <meta property="og:description"        content="{{ $post->excerpt }}" />
        <meta property="og:image"              content="{{ url("") }}{{ config('settings.image.post.slider.upload_dir').$photo->filename }}" />
        <meta property="fb:app_id"             content="1689404694439169" />
    @endif

    @yield('styles')
    
    <style>
    table td {
        border: none;
    }
    </style>

    <link rel="stylesheet" href="/assets/front/dist/styles/screen.min.css?a">


    <script>
        window.fbAsyncInit = function() {
          FB.init({
            appId      : '1689404694439169',
            xfbml      : true,
            version    : 'v2.12'
          });
          FB.AppEvents.logPageView();
        };
      
        (function(d, s, id){
           var js, fjs = d.getElementsByTagName(s)[0];
           if (d.getElementById(id)) {return;}
           js = d.createElement(s); js.id = id;
           js.src = "https://connect.facebook.net/en_US/sdk.js";
           fjs.parentNode.insertBefore(js, fjs);
         }(document, 'script', 'facebook-jssdk'));
      </script>

        <script>
            var csrf = '{{ csrf_token() }}';
            var basePath = '{{ url("") }}';
        </script>
</head>
<body>
    <div id="barba-wrapper">
        
        
        @include('front.layout._partials.header')
        
        <div class="barba-container" data-namespace="{{ $routeName }}">
            @yield('content')
        </div>
        
    </div>
    @include('front.layout._partials.footer')
    

    <div id="preloader">
        <img src="/assets/front/dist/images/preloader.gif" alt="">
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js'></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/barba.js/1.0.0/barba.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

    @yield('scripts')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQcNzU8G9Yl_oGe0kArJmM9DKd93147Xw&callback=initMap"></script>
    
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/lightgallery@1.6.5/dist/js/lightgallery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/lightgallery@1.6.7/modules/lg-thumbnail.min.js"></script>
    <script src="/assets/front/dist/scripts/main.min.js?a"></script>

    <script>
        // $(document).on('click', '.career-btn, .contact-btn', function() {
        //     alert('Email servis trenutno nije u funkciji. Pokušajte kasnije, hvala.')
        // })


       
    </script>
</body>
</html>
