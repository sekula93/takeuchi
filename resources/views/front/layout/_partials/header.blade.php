<section class="custom-navbar">
    <div class="container">
        <nav class="navbar navbar-toggleable-md navbar-light">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        <a class="navbar-brand" href="{{ route('front.home.index') }}"><img src="/assets/front/dist/images/logo2.png" alt=""></a>
            
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item {{ $routeName == 'front.home.index' ? 'active' : ''}}">
                        <a class="nav-link" href="{{ route('front.home.index') }}">Početna</a>
                    </li>
                    <li class="nav-item {{ $routeName == 'front.company.index' || $routeName == 'front.gallery.index' ? 'active' : ''}}">
                        <a class="nav-link" href="{{ route('front.company.index') }}">Kompanija</a>
                    </li>
                    <li class="nav-item {{ $routeName == 'front.product.index' || $routeName == 'front.product.list' || $routeName == 'front.product.show'  ? 'active' : ''}}">
                        <a class="nav-link" href="{{ route('front.product.index') }}">Proizvodi</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="#">Dodatna oprema</a>
                    </li> -->
                    <li class="nav-item {{ $routeName == 'front.rental.list' || $routeName == 'front.rental.show'  ? 'active' : ''}}">
                        <a class="nav-link" href="{{ route('front.rental.list') }}">Rental</a>
                    </li>
                    <li class="nav-item {{ $routeName == 'front.used.list' || $routeName == 'front.used.show'  ? 'active' : ''}}">
                        <a class="nav-link" href="{{ route('front.used.list') }}">Polovne mašine</a>
                    </li>
                    <li class="nav-item {{ $routeName == 'front.service.show'  ? 'active' : ''}}">
                        <a class="nav-link" href="{{ route('front.service.show') }}">Servis i delovi</a>
                    </li>
                    <li class="nav-item {{ $routeName == 'front.post.index'  ? 'active' : ''}}">
                        <a class="nav-link" href="{{ route('front.post.index') }}">Vesti</a>
                    </li>
                    <li class="nav-item {{ $routeName == 'front.contact.index'  ? 'active' : ''}}">
                        <a class="nav-link" href="{{ route('front.contact.index') }}">Kontakt</a>
                    </li>
                    {{--  <li class="nav-item">
                        <a class="nav-link" href="#">Kontakt</a>
                    </li>  --}}
                </ul>
            </div>
        </nav>
        
    </div>
</section>