<html>
<head>
    <style>
        p {
            margin: 0;
        }
    </style>
</head>
<body>
    <p>Poštovani,</p>
    <br>
    <p>{{$content}}</p>
    <br>
    <p>Srdačan pozdrav,</p>
    <p>{{ $name }}</p>
    <p>{{ $email }}</p>
    <p>{{ $phone }}</p>
    @if(isset($daterange))
    <p>{{ $daterange  }}</p>
    <p>{{ $model }}</p>
    @endif
</body>
</html>