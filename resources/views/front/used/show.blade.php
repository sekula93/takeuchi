@extends('front.layout.template')
@section('content')

<section class="single-post single-product">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="post-slider">
                    <div class="slider-item text-center">
                        <img class="" src="{{ config('settings.image.product.upload_dir').$item->filename }}" alt="">
                    </div>
                    @if($item->photos)
                        @foreach ($item->photos as $photo)
                        <div class="slider-item text-center">
                            <img class="" src="{{ config('settings.image.product.slider.upload_dir').$photo->filename }}" alt="">
                        </div>
                        @endforeach
                    @endif
                </div>
                @if (count($item->photos))
                <div class="arrows">
                    <span class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
                    <div class="divider"></div>
                    <span class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                </div>      
                @endif
            </div>
            <div class="row flex-column single-post--content">
                <div class="single-post--title justify-content-between d-flex align-items-center">
                    <h1>{{ $item->title }}</h1>
                    <h4>Polovne mašine</h4>
                </div>
                <!-- <div class="single-post--date">
                    <div class="left">
                        <span class="text">Datum objave:</span>
                        <span class="date">26.12.2017.</span>
                    </div>
                </div> -->
                <div class="single-post--text">
                    {!! $item->text !!}
                </div>
                
                <div class="single-product-table d-flex align-items-center flex-column">
                    @foreach ($item->specifications as $specification)
                        {!! $specification->text !!}
                    @endforeach
                    {{--  <div class="note">
                        <p>*neki note ako bude trebalo</p>
                    </div>  --}}
                    @if ($item->attachment)
                    <div class="catalogue">
                        <a href="{{ config('settings.file.product.upload_dir').$item->attachment }}" target="_blank"  class="button button-more"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Preuzmi katalog</a>
                    </div>
                    @endif
                </div>
                {{--  {{ $item->specifications[0]->relatedProducts }}  --}}
                {{--  @foreach($item->specifications[0]->relatedProducts as $product)
                    {{ $product->productSubcategory->slug }} <br> <br>
                @endforeach   --}}
                @if (isset($item->specifications[0]->relatedProducts) && count($item->specifications[0]->relatedProducts) > 0)
                <div class="compatible-slider">
                    @foreach($item->specifications[0]->relatedProducts as $product)
                    <div class="slider-item text-center">
                       <a href="{{ route('front.used.show', $product->slug) }}">
                           <img class="" src="{{ config('settings.image.product.upload_dir').$product->filename }}" alt="">
                        </a>
                       <h5>{{ $product->title }}</h5>
                    </div>
                    @endforeach
                </div>
                @endif
            </div>
                <div class="share d-flex align-items-center">
                    <span class="text">Podeli na mreži </span>
                    <i class="fa fa-facebook-square" aria-hidden="true"></i>
                </div>
            </div>
            <div class="row">
        </div>
    </section>
@stop
