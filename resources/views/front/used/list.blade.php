@extends('front.layout.template')
@section('content')
    
<section class="titlebar d-flex justify-content-center align-items-center" style="background-image: url('/assets/front/dist/images/construction_gallery_bg.jpg')">
    <div class="page-name text-center">
        <h1 class="red bold">Polovne mašine</h1>
        {{--  <p>{{ $subcat->text }}</p>  --}}
    </div>
</section>
{{--  <section class="filter-bar d-flex align-items-center">
    <div class="container">
        <select name="select">
            <option value="1">-- Sortiraj --</option>
            <option value="2">Po nazivu</option>
        </select>
    </div>
</section>  --}}
<section class="product-list">
    <div class="container">
        <div class="row">
            @if(count($products))
                @foreach($products as $product)
                <div class="col-md-6">
                    <div class="d-flex product-item">
                        <div class="col-md-6">
                            <img class="product-image" src="{{ config('settings.image.product.upload_dir').$product->filename }}" alt="">
                        </div>
                        <div class="col-md-6 d-flex flex-column justify-content-between">
                            <div class="product-title">
                                <h2 class="model red">{{ $product->title }}</h2>
                                <!-- <h3 class="title">Bager mini</h3> -->
                            </div>
                            {{--  tabela  --}}
                            <div class="product-info"> 
                                @if (count($product->specifications) > 1)

                                @else
                                    @if(isset($product->specifications[0]->text))
                                        {!! $product->specifications[0]->text !!}
                                    @endif
                                @endif
                            </div>
                            <div class="read-more align-self-end">
                                <a href="{{ route('front.used.show', $product->slug) }}" title="" class="button button-more">Pročitaj više</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            @else
                <h2>Stranica u pripremi</h2>
            @endif
        </div>
        {{ $products->links('front.pagination.index') }}
    </div>
</section>


@stop