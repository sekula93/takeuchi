@extends('front.layout.template')
@section('content')
<section class="company">
    <div class="container-fluid">
        @foreach($companies as $key => $item)
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 picture {{ ($key+1) % 2 == 0 ? 'flex-last' : 'flex-first' }} ">
                    <img class="p-5" src="{{ config('settings.image.post.upload_dir').$item->filename }}" alt="">
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 description {{ ($key+1) % 2 == 0 ? 'flex-first' : 'flex-last' }}">
                    <h1>{{ $item->title }}</h1>
                    <p>{{ $item->excerpt }}</p>
                    <a href="{{ route('front.company.show', $item->slug) }}" class="button button-more" title="">Pročitaj više</a>
                </div>
            </div>
        @endforeach
           
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 picture flex-first">
                <img src="/assets/front/dist/images/gallery.jpg" alt="">
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 description flex-last">
                <h1>Galerija</h1>
                <p>{{ $pictures[0]->excerpt }}</p>
                <a href="{{ route('front.gallery.index') }}" class="button button-more" title="">Pročitaj više</a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 picture flex-last">
                <img src="/assets/front/dist/images/career.jpg" alt="">
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 description flex-first">
                <h1>Zaposlenje</h1>
                <a href="{{ route('front.career.index') }}" class="button button-more" title="">Pročitaj više</a>
            </div>
        </div>
    <!-- </div> -->
</section>
@stop