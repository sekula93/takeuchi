@extends('front.layout.template')
@section('content')
    
<section class="products">
    <div class="container">
        <div class="product-list">
            @foreach($categories as $category)
                @if(count($category->productSubcategories) > 1)
                    <div class="list-item">
                        <div class="main-list-item">
                            <img src="{{ config('settings.image.product-category.upload_dir').$category->filename }}" alt="">
                            <h2 class="red">{{ $category->name }}</h2>
                        </div>
                                            
                        <div class="sub-list flex-wrap">
                            @foreach($category->productSubcategories as $subcategory)
                                <a href="{{ route('front.product.list', $subcategory->slug) }}" class="sub-item">{{ $subcategory->name }}</a>
                            @endforeach
                        </div>
                    </div>
                @else
                    @if(isset($category->productSubcategories[0]))
                        @if($category->productSubcategories[0]->flag)
                            @if(isset($category->productSubcategories[0]->products[0]))
                            <a href="{{ route('front.product.show', ['cat' => $category->productSubcategories[0]->slug, 'slug' => $category->productSubcategories[0]->products[0]->slug]) }}">
                            @endif
                        @else
                            <a href="{{ route('front.product.list', $category->productSubcategories[0]->slug) }}">
                        @endif
                    @else
                        <a href="#"></a>
                    @endif
                        <div class="main-list-item">
                            <img src="{{ config('settings.image.product-category.upload_dir').$category->filename }}" alt="">
                            <h2 class="red">{{ $category->name }}</h2>
                        </div>
                    </a>
                @endif
            @endforeach
        </div>
    </div>
</section>


@stop