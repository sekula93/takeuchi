@extends('front.layout.template')
@section('content')
    
<section class="titlebar d-flex justify-content-center align-items-center" style="background-image: url('/assets/front/dist/images/construction_gallery_bg.jpg')">
    <div class="page-name text-center">
        <h1 class="red bold">{{ $subcat->name }}</h1>
        <p>{{ $subcat->text }}</p>
    </div>
</section>
{{--  <section class="filter-bar d-flex align-items-center">
    <div class="container">
        <select name="select">
            <option value="1">-- Sortiraj --</option>
            <option value="2">Po nazivu</option>
        </select>
    </div>
</section>  --}}
<section class="product-list">
    <div class="container">
        <div class="row">
            @if($products)
                @foreach($products as $product)
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="d-flex product-item">
                        <div class="col-md-6 col-xs-12">
                            <img class="product-image" src="{{ config('settings.image.product.upload_dir').$product->filename }}" alt="">
                        </div>
                        <div class="col-md-6 col-xs-12 d-flex flex-column justify-content-between align-items-center">
                            <div class="product-title">
                                <h2 class="model red">{{ $product->title }}</h2>
                                <!-- <h3 class="title">Bager mini</h3> -->
                            </div>
                            {{--  tabela  --}}
                            @if ($subcat->id != 12)
                                <div class="product-info"> 
                                    @if (count($product->specifications) > 1)

                                    @else
                                        @if(isset($product->specifications[0]->text))
                                            {!! $product->specifications[0]->text !!}
                                        @endif
                                    @endif
                                </div>
                            @endif
                            <div class="read-more align-self-end">
                                <a href="{{ route('front.product.show', ['cat' => $subcat->slug, 'slug' => $product->slug]) }}" title="" class="button button-more">Pročitaj više</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                
            @endif
        </div>
        {{ $products->links('front.pagination.index') }}
    </div>
</section>


@stop