@extends('front.layout.template')
@section('content')
<section class="titlebar d-flex justify-content-center align-items-center" style="background-image: url('/assets/front/dist/images/construction_gallery_bg.jpg')">
    <div class="page-name text-center">
        <h1 class="red bold">Vesti</h1>
        {{--  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem perferendis fuga fugit voluptate,<br> ea blanditiis et ad alias! Eligendi ea quas veniam recusandae? Culpa, ducimus. Voluptatibus cum quam illum ex!</p>  --}}
    </div>
</section>

<section class="posts">
        <div class="container">
            <!-- <h4 class="section-title">Najnovije vesti</h4> -->
            <!-- <div class="section-title--divider"></div> -->
            <div class="post-wrap">
                <div class="row">
                    @if($posts)
                        @foreach($posts as $post)
                        <div class="col-md-4">
                            <div class="post-item">
                                <div class="post-image">
                                    <img src="{{ config('settings.image.post.upload_dir').$post->filename }}" alt="">
                                    <div class="post-image--overlay">
                                        <a href="{{ route('front.post.show', $post->slug) }}" class="button button-post">Detaljnije</a>
                                    </div>
                                </div>
                                <div class="title">
                                    <h4 class="post-title">{{ $post->title }}</h4 class="post-title">
                                </div>
                                <div class="text">
                                    <p>{{ $post->excerpt }}</p>
                                </div>
                                <div class="date">
                                    <p class="post-date text-right">{{ date('d.m.Y', strtotime($post->created_at)) }}</p>
                                </div>
                                @if($post->category->slug == 'akcija')
                                    <div class="action">
                                        <p>{{ $post->category->name }}</p>
                                    </div>
                                @endif
                            </div>
                        </div>
                        @endforeach
                    @endif
                </div>
            </div>
            @if($posts)
                {{ $posts->links('front.pagination.index') }}
            @endif
        </div>
        <!-- container -->
    </section>
@stop