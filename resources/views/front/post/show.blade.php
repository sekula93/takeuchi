@extends('front.layout.template')
@section('content')
<section class="single-post">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="post-slider">
                
                    @if(count($post->photos) > 0)
                        @foreach($post->photos as $photo)
                        <div class="slider-item">
                            <img class="" src="{{ config('settings.image.post.slider.upload_dir').$photo->filename }}" alt="">
                        </div>
                        @endforeach
                    @else
                        <div class="slider-item">
                            <img class="" src="{{ config('settings.image.post.upload_dir').$post->filename }}" alt="">
                        </div>
                    @endif
                </div>
                @if(count($post->photos) > 0)
                    <div class="arrows">
                        <span class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
                        <div class="divider"></div>
                        <span class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                    </div>   
                @endif   
            </div>
            <div class="row flex-column single-post--content">
                <div class="single-post--title">
                    <h1>{{ $post->title }}</h1>
                </div>
                @if($post->flag)
                <div class="single-post--date">
                    <div class="left">
                        <span class="text">Datum objave:</span>
                        <span class="date">26.12.2017.</span>
                    </div>
                </div>
                @endif
                <div class="single-post--text">
                    {!! $post->text !!}
                </div>
                <div class="share d-flex align-items-center">
                    <span class="text">Podeli na mreži </span>
                    <i class="fa fa-facebook-square" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </section>
@stop