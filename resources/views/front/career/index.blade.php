@extends('front.layout.template')
@section('content')
<section class="titlebar d-flex justify-content-center align-items-center" style="background-image: url('/assets/front/dist/images/construction_gallery_bg.jpg')">
    <div class="page-name text-center">
        <h1 class="red bold">Zaposlenje</h1>
    </div>
</section>
<section class="contact-form">
    <div class="container">
        <div class="wrap">
            <form id="needs-validation" class="career-form" enctype="multipart/form-data" novalidate>
                <div class="row">
                    <div class="col-md-6 col-xs-12  form-wrap form-group">
                        <div class="mb-4">
                          <input type="text" class="form-control" name="name" placeholder="Ime i prezime*" required>
                        </div>
                        <div class="mb-4"> 
                          <input type="email" class="form-control" name="email" placeholder="Email*" required>
                        </div>
                        <div class=""> 
                          <input type="tel" class="form-control" name="phone" placeholder="Broj telefona (opciono)" required>
                        </div>
                        {{ csrf_field() }}
                    </div>
                    <div class="col-md-6 col-sm-12 form-group">
                        <textarea placeholder="Tekst*" class="form-control" name="text" id="exampleFormControlTextarea1" rows="6"></textarea>
                    </div>    
                </div>
                <!-- END ROW -->
                <div class="button-wrap d-flex justify-content-between">
                    <div>
                        <label style="margin-bottom: 0; cursor: pointer;" class="button button-more" for="attachment">Upload CV</label>
                        <p class="attach-name"></p>
                        <input style="display: none" id="attachment" name="attachment" type="file">
                    </div>
                    <div>
                        <button class="button button-more career-btn" type="submit">Pošalji</button>
                    </div>
                    <p class="message text-center"></p>
                </div>
            </form>
        </div>
        <!-- END WRAP -->
        <div class="row">
            <div class="col-md-12 col-xs-12 text-center address-wrap">
                <h4 class="title">Takeuchi-Srbija - Regionalna centrala Novi Sad</h4>
                <p class="address"><span class="label">Adresa: </span> Ogledna polja 55,  21233 Čenej</p>
                <p class="contact"><span class="label">Kontakt telefon: </span> +381 21 2 100 650</p>
                <p class="contact"><span class="label">Prodaja I iznajmljivanje: </span> +381 60 88 11 006 / +381 60 88 11 045 </p>
                <p class="contact"><span class="label">Servis:</span> +381 60 88 11 025</p>
                <p class="contact"><span class="label">Rezervni delovi:</span> +381 60 88 11 035</p>
                <p class="contact"><span class="label">Direktor:</span> +381 60 88 11 006</p>
                <br>
                <p class="contact"><span class="label">Generalne informacije: </span> info@takeuchi-srbija.com</p>
                <p class="contact"><span class="label">Servis i podrška: </span> servis@takeuchi-srbija.com</p>
                <p class="contact"><span class="label">Prodaja: </span> prodaja@takeuchi-srbija.com / aleksandar.mihailovic@takeuchi-srbija.com</p>
            </div>
        </div>
    </div>
    <!-- END CONTAINER -->
</section>
  <!-- END SECTION.CONTACT-FORM -->
@stop