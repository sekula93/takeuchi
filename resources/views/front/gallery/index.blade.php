@extends('front.layout.template')

@section('styles')
    {{--  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/lightgallery@1.6.5/dist/css/lightgallery.min.css">  --}}
@stop

@section('content')
    <section class="gallery-hero d-flex justify-content-center align-items-center" style="background-image: url('/assets/front/dist/images/construction_gallery_bg.jpg')">
        <div class="page-name">
            <h1 class="red bold">Galerija</h1>
        </div>
    </section>
    <section class="gallery">
        <div class="container">
            <div id="lightgallery">
                @if($pictures)
                    @foreach($pictures as $picture)
                        @foreach($picture->photos as $photo)
                            <a class="no-barba" href="{{ config('settings.image.gallery.upload_dir').$photo->filename }}" class="">
                                <img class="" src="{{config('settings.image.gallery.upload_dir').$photo->filename}}" />
                            </a>
                        @endforeach
                    @endforeach
                @endif
            </div>
        </div>
    </section>
@stop

@section('scripts')
    {{--  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/lightgallery@1.6.5/dist/js/lightgallery.min.js"></script>  --}}
    {{--  <script src="https://cdn.jsdelivr.net/npm/lightgallery@1.6.7/modules/lg-thumbnail.min.js"></script>  --}}
@stop