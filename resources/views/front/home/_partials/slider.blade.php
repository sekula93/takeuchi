<section class="hero">
    <div class="slider">
        @if($slides)
            @foreach($slides as $slide)
            <div class="slider-item" style="background-image: url( {{ config('settings.image.slide.upload_dir').$slide->filename }} )">
                <div class="slider-item--text">
                    <div class="title">
                        <h2>{{ $slide->title }}</h2>
                    </div>
                    <div class="text">
                        @if($slide->subtitle)
                            <p>{{ $slide->subtitle }}</p> 
                        @endif
                    </div>
                    @if($slide->link)
                        <a class="button button-more" href="{{ $slide->link }}">pročitaj</a>
                    @endif
                </div>
            </div>
            @endforeach
        @endif
       
    </div>
    <span class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
    <span class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
</section>