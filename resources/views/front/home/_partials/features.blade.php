<section class="features text-center">
    <div class="container">
        <div class="row justify-content-center">
            {{--  @foreach ($services as $service)  --}}
                <div class="col-md-8 col-sm-8 col-xs-8">
                    {{--  <h4 class="features-title">{{$services->first()->title}}</h4>  --}}
                    <h4 class="features-text text-center">{{ $services->first()->subtitle }}</h4>
                    <div class="features-divider mt-4"></div>
                </div>
            {{--  @endforeach  --}}
        </div>
    </div>
</section>