<section class="posts">
    <div class="container">
        <h4 class="section-title">Najnovije vesti</h4>
        <div class="section-title--divider"></div>
        <div class="post-wrap">
            <div class="row">
                @foreach($recentPosts as $post)
                <div class="col-md-4">
                    <div class="post-item">
                        <div class="post-image">
                            <img src="{{ config('settings.image.post.upload_dir').$post->filename }}" alt="">
                            <div class="post-image--overlay">
                                <a href="{{ route('front.post.show', $post->slug) }}" class="button button-post">Detaljnije</a>
                            </div>
                        </div>
                        <div class="title">
                            <h4 class="post-title">{{ $post->title }}</h4 class="post-title">
                        </div>
                        <div class="text">
                            <p class="post-text">{{ $post->excerpt }}</p>
                        </div>
                        <div class="date">
                            <p class="post-date text-right">{{ date('d.m.Y', strtotime($post->created_at)) }}</p>
                        </div>
                        @if($post->category->slug == 'akcija')
                            <div class="action">
                                <p>{{ $post->category->name }}</p>
                            </div>
                        @endif
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>