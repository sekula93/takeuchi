<section class="clients">
    <div class="client-title text-center">
        <h3>Naši saradnici</h3>
        <div class="title-divider"></div>
    </div>
    <div class="client-box">
        @if($associates)
            @foreach($associates as $associate)
            <img class="client-image" src="{{ config('settings.image.associates.upload_dir').$associate->filename }}" alt="">

            @endforeach
        @endif
    </div>
</section>