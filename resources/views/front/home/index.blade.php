@extends('front.layout.template')
@section('content')
    @include('front.home._partials.slider')

    @include('front.home._partials.features')
    
    @include('front.home._partials.clients')

    @include('front.home._partials.recent-posts')
@stop