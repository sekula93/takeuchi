@extends('front.layout.template')
@section('content')

<section class="single-post single-product">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="post-slider">
                    <div class="slider-item text-center">
                        <img class="" src="{{ config('settings.image.product.upload_dir').$item->filename }}" alt="">
                    </div>
                    @if($item->photos)
                        @foreach ($item->photos as $photo)
                        <div class="slider-item text-center">
                            <img class="" src="{{ config('settings.image.product.slider.upload_dir').$photo->filename }}" alt="">
                        </div>
                        @endforeach
                    @endif
                </div>
                @if (count($item->photos))
                <div class="arrows">
                    <span class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
                    <div class="divider"></div>
                    <span class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                </div>      
                @endif
            </div>
            <div class="row flex-column single-post--content">
                <div class="single-post--title justify-content-between d-flex align-items-center">
                    <h1>{{ $item->title }}</h1>
                    <h4>Rental</h4>
                </div>
                <!-- <div class="single-post--date">
                    <div class="left">
                        <span class="text">Datum objave:</span>
                        <span class="date">26.12.2017.</span>
                    </div>
                </div> -->
                <div class="single-post--text">
                    {!! $item->text !!}
                </div>
                
                <div class="single-product-table d-flex align-items-center flex-column">
                    @foreach ($item->specifications as $specification)
                        {!! $specification->text !!}
                    @endforeach
                    {{--  <div class="note">
                        <p>*neki note ako bude trebalo</p>
                    </div>  --}}
                    @if ($item->attachment)
                    <div class="catalogue">
                        <a href="{{ config('settings.file.product.upload_dir').$item->attachment }}" target="_blank"  class="button button-more"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Preuzmi katalog</a>
                    </div>
                    @endif
                </div>
                {{--  {{ $item->specifications[0]->relatedProducts }}  --}}
                {{--  @foreach($item->specifications[0]->relatedProducts as $product)
                    {{ $product->productSubcategory->slug }} <br> <br>
                @endforeach   --}}
                @if (isset($item->specifications[0]->relatedProducts) && count($item->specifications[0]->relatedProducts) > 0)
                <div class="compatible-slider">
                    @foreach($item->specifications[0]->relatedProducts as $product)
                    <div class="slider-item text-center">
                       <a href="{{ route('front.rental.show', $product->slug) }}">
                           <img class="" src="{{ config('settings.image.product.upload_dir').$product->filename }}" alt="">
                        </a>
                       <h5>{{ $product->title }}</h5>
                    </div>
                    @endforeach
                </div>
                @endif
            </div>
                <div class="share d-flex align-items-center">
                    <span class="text">Podeli na mreži </span>
                    <i class="fa fa-facebook-square" aria-hidden="true"></i>
                </div>
            </div>
            <div class="row">
        </div>
    </section>
    <section class="contact-form">
        <div class="container">
            <h2 class="text-center">Pošalji upit</h2>
            <div class="wrap">
                <form id="needs-validation" class="rental-form" novalidate enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6 col-xs-12  form-wrap form-group">
                            <div class="mb-4">
                              <input type="text" class="form-control" placeholder="Ime i prezime*" name="name" required>
                            </div>
                            <div class="mb-4"> 
                              <input type="email" class="form-control" placeholder="Email*" name="email" required>
                            </div>
                            <div class="mb-4"> 
                                <input type="text" class="form-control" name="daterange">
                            </div>  
                            <div class=""> 
                              <input type="text" class="form-control" placeholder="Broj telefona (opciono)" name="phone" >
                            </div>
                            <input type="hidden" name="model" value="{{ $item->title }}" >
                            {{ csrf_field() }}
                        </div>
                        <div class="col-md-6 col-sm-12 form-group">
                            <textarea placeholder="Tekst*" class="form-control" name="text" rows="6"></textarea>
                        </div>    
                    </div>
                    <!-- END ROW -->
                    <button class="button button-more rental-btn" type="submit">Pošalji</button>
                    <p class="message text-center"></p>
                </form>
            </div>
        </div>
        <!-- END CONTAINER -->
    </section>
@stop
