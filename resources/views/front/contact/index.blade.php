@extends('front.layout.template')
@section('content')
<section class="titlebar d-flex justify-content-center align-items-center" style="background-image: url('/assets/front/dist/images/construction_gallery_bg.jpg')">
    <div class="page-name text-center">
        <h1 class="red bold">Kontakt </h1>
    </div>
</section>
<section class="contact-form">
    <div class="container">
        <div class="wrap">
            <form id="needs-validation" novalidate>
                <div class="row">
                    <div class="col-md-6 col-xs-12  form-wrap form-group">
                        <div class="mb-4">
                          <input type="text" class="form-control" placeholder="Ime i prezime*" name="name" required>
                        </div>
                        <div class="mb-4"> 
                          <input type="email" class="form-control" placeholder="Email*" name="email" required>
                        </div>
                        <div class=""> 
                          <input type="text" class="form-control" placeholder="Broj telefona (opciono)" name="phone" required>
                        </div>
                        {{ csrf_field() }}
                    </div>
                    <div class="col-md-6 col-sm-12 form-group">
                        <textarea placeholder="Tekst*" class="form-control" id="exampleFormControlTextarea1" name="text" rows="6"></textarea>
                    </div>    
                </div>
                <!-- END ROW -->
                <button class="button button-more contact-btn" type="submit">Pošalji</button>
                <p class="message text-center"></p>
            </form>
        </div>
        <!-- END WRAP -->
        <div class="row">
            <div class="col-md-12  col-xs-12 text-center address-wrap">
                <h4 class="title">Takeuchi-Srbija - Regionalna centrala Novi Sad</h4>
                <p class="address"><span class="label">Adresa: </span> Ogledna polja 55,  21233 Čenej</p>
                <p class="contact"><span class="label">Kontakt telefon: </span> +381 21 2 100 650</p>
                <p class="contact"><span class="label">Prodaja I iznajmljivanje: </span> +381 60 88 11 006 / +381 60 88 11 045 </p>
                <p class="contact"><span class="label">Servis:</span> +381 60 88 11 025</p>
                <p class="contact"><span class="label">Rezervni delovi:</span> +381 60 88 11 035</p>
                <p class="contact"><span class="label">Direktor:</span> +381 60 88 11 006</p>
                <br>
                <p class="contact"><span class="label">Generalne informacije: </span> info@takeuchi-srbija.com</p>
                <p class="contact"><span class="label">Servis i podrška: </span> servis@takeuchi-srbija.com</p>
                <p class="contact"><span class="label">Prodaja: </span> prodaja@takeuchi-srbija.com / aleksandar.mihailovic@takeuchi-srbija.com</p>

            </div>
        </div>
    </div>
    <!-- END CONTAINER -->
</section>
@stop