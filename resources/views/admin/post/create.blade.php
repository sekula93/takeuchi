@extends('admin.layout.template')

@section('page-heading', 'Blog Post')

@section('content')
    <div class="panel-heading">
        <h4>Novi post</h4>
        <div class="options">

        </div>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" action="{{ route('admin.post.store') }}" data-resource="post" method="post" enctype="multipart/form-data">                
            {{ csrf_field() }}
            <div class="form-group">
                <div class="row">
                    <label for="title" class="col-sm-2 control-label">Naslov</label>
                    <div class="col-sm-6">
                        <textarea class="form-control slugify" id="title" name="title"></textarea>
                    </div>
                    <div class="col-sm-3">
                        <p class="help-block">{{ $errors->first('title') }}</p>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label for="slug" class="col-sm-2 control-label">Slug</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="slug" name="slug">
                    </div>
                    <div class="col-sm-3">
                        <p class="help-block">{{ $errors->first('slug') }}</p>
                    </div>
                </div>
            </div>
            <div class="form-group">
                    <div class="row">
                        <label for="category-select" class="col-sm-2 control-label">Kategorija</label>
                        <div class="col-sm-6">
                            <select id="category-select" class="form-control" name="post_category_id">
                                <option value="-1" selected >Izaberi kategoriju</option>
                                @if($categories)
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <p class="help-block">{{ $errors->first('post_category_id') }}</p>
                        </div>
                    </div>
                </div>
            <div class="form-group">
                <div class="row">    
                    <label for="excerpt" class="col-sm-2 control-label">Uvod</label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="excerpt" name="excerpt"></textarea>
                    </div>
                    <div class="col-sm-3">
                        <p class="help-block">{{ $errors->first('excerpt') }}</p>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">    
                    <label for="text" class="col-sm-2 control-label">Tekst</label>
                    <div class="col-sm-6">
                        <textarea rows="6" class="form-control" id="text" name="text"></textarea>
                    </div>
                    <div class="col-sm-3">
                        <p class="help-block">{{ $errors->first('text') }}</p>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-5 col-sm-offset-2">  
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 360px; height: 200px;"></div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 360px; max-height: auto;"></div>
                            <div>
                                <span class="btn btn-default btn-file"><span class="fileinput-new">Izaberi sliku</span><span class="fileinput-exists">Izmeni</span>
                                <input type="file" name="file" accept="image/*"></span>
                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Izbriši</a>
                            </div>
                        </div>
                        <p class="help-block">{{ $errors->first('file') }}</p>
                        <p>Preporučena dimenzija: min. <b>1300x700px<b></p>
                    </div>
                    <div class"col-sm-3">
                        <label for="post_date" class="control-label">Datum objave:</label>
                        <input type="date" id="post_date" name="post_date">
                        <p class="help-block">{{ $errors->first('post_date') }}</p>                        
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-4 col-md-offset-3">
                    <input type="hidden" name="is_active" value="0">
                    <div class="toggle toggle-success form-active"></div>
                </div>
                {{ $errors->first('is_active') }}
                <div class="col-sm-3">
                    <button class="btn-primary btn">Snimi</button>                    
                </div>
            </div>
        </form>
    </div>
@stop