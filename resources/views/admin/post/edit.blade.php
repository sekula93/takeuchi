@extends('admin.layout.template')
@section('page-heading', $post->flag ? 'Post' : 'Kompanija')

@section('content')
    @include('flash::message')

    <div class="panel-heading">
        
        {{--  <h4>Uredi post</h4>  --}}
        <div class="options">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#form" data-toggle="tab">Forma</a>
                </li>
                <li>
                    <a href="#packshots" data-toggle="tab">Fotografije</a>
                </li>
            </ul>    
        </div>
    </div>
    <div class="panel-body">
        <div class="tab-content">
            <div class="tab-pane active" id="form">
                <form class="form-horizontal" action="{{ route('admin.post.update', $post->id) }}" data-resource="post" method="post" enctype="multipart/form-data">                
                        {{ csrf_field() }}
                        {{ method_field('put') }}
        
                        <div class="form-group">
                            <div class="row">
                                <label for="title" class="col-sm-2 control-label">Naslov</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control slugify" id="title" name="title">{{ $post->title }}</textarea>
                                </div>
                                <div class="col-sm-3">
                                    <p class="help-block">{{ $errors->first('title') }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label for="slug" class="col-sm-2 control-label">Slug</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="slug" name="slug" value="{{ $post->slug }}">
                                </div>
                                <div class="col-sm-3">
                                    <p class="help-block">{{ $errors->first('slug') }}</p>
                                </div>
                            </div>
                        </div>
                        @if($post->flag)
                        <div class="form-group">
                            <div class="row">
                                <label for="category-select" class="col-sm-2 control-label">Kategorija</label>
                                <div class="col-sm-6">
                                    <select id="category-select" class="form-control" name="post_category_id">
                                        @if($categories)
                                            @foreach($categories as $category)
                                                <option {{ $post->post_category_id == $category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        @else 
                            <input type="hidden" name="post_category_id" value="{{ $post->post_category_id }}">
                        @endif
                        @if($post->id != 3)
                        <div class="form-group">
                            <div class="row">    
                                <label for="excerpt" class="col-sm-2 control-label">Uvod</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control" id="excerpt" name="excerpt">{{ $post->excerpt }}</textarea>
                                </div>
                                <div class="col-sm-3">
                                    <p class="help-block">{{ $errors->first('excerpt') }}</p>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="form-group">
                            <div class="row">    
                                <label for="text" class="col-sm-2 control-label">Tekst</label>
                                <div class="col-sm-6">
                                    <textarea rows="6" class="form-control" id="text" name="text">{{ $post->text }}</textarea>
                                </div>
                                <div class="col-sm-3">
                                    <p class="help-block">{{ $errors->first('text') }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-5 col-sm-offset-2">  
                                    <div class="fileinput fileinput-exists" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 360px; height: 200px;"></div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 360px; max-height: auto;">
                                            <img src="{{ config('settings.image.post.upload_dir').$post->filename }}" class="img-responsive" alt="">
                                        </div>
                                        <div>
                                            <span class="btn btn-default btn-file"><span class="fileinput-new">Izaberi sliku</span><span class="fileinput-exists">Izmeni</span>
                                            <input type="file" name="file" accept="image/*"></span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Izbriši</a>
                                        </div>
                                    </div>
                                    {{ $errors->first('file') }}
                                    <p>Preporučena dimenzija: min. <b>1300x700px</b></p>
                                    @if($post->flag)
                                    <div class"col-sm-3">
                                        <label for="post_date" class="control-label">Datum objave:</label>
                                        <input type="date" id="post_date" name="post_date" value="{{ date('Y-m-d', strtotime($post->created_at)) }}">
                                    </div>
                                    @else
                                        <input type="hidden" id="post_date" name="post_date" value="{{ date('Y-m-d', strtotime($post->created_at)) }}">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-3">
                                <input type="hidden" name="is_active" value="{{ $post->is_active }}">
                                <div class="toggle toggle-success {{ $post->is_active ? 'active' : '' }} form-active" title="Status" data-id="{{ $post->id }}"></div>
                            </div>
                            {{ $errors->first('is_active') }}
                            <div class="col-sm-3">
                                <button class="btn-primary btn">Snimi</button>                    
                            </div>
                        </div>
                </form>
            </div>
            <div class="tab-pane" id="packshots">
                {{--  <form action="#" class="fileupload-photo" >  --}}
                    <form class="fileupload-photo" action="{{ route('admin.post-photo.store', $post->id) }}" method="POST" enctype="multipart/form-data">
                        <span class="btn btn-success fileinput-button photo-upload-button">
                            <h2><i class="fa fa-cloud-upload"></i>Spustite fajlove ovde</h2>
                            <h4>ili kliknite</h4>
                            <!-- The file input field used as target for the file upload widget -->
                            <input type="file" name="files[]" multiple>
                        </span>
                    </form>
                    <br>
                    <br>
                    <!-- The global progress bar -->
                    <div class="progress-bar progress-bar-success col-md-12" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100" style="">
                        <span class="sr-only"></span>
                    </div>
                    <div class="col-md-12 errors-wrapper"></div>
            
                    <ul class="post images gallery uploaded list-unstyled col-sm-12" data-type="1">
                        @if($post->photos)
                        @foreach ($post->photos as $photo)
                        <li class="image" data-resource="post-photo" data-id="{{ $photo->id }}" id="file-{{ $photo->id }}">
                            <a class="file-thumb {{ !$photo['is_active'] ? 'inactive' : ''}}" href="/uploads/posts/images/{{ $photo->filename }}" data-lightbox="photos">
                                <img src="/uploads/posts/images/{{ $photo->filename }}" width="230"></a>
                            </a>
                            <div class="toggle toggle-success {{ $photo->is_active ? 'active' : '' }} file-active" title="Status"></div>
                            <button type="button" class="btn btn-danger-alt remove-file"><i class="fa fa-times"></i></button>
                        </li>
                        @endforeach
                        @endif
                    </ul>
                
                {{--  </form>  --}}
            </div>
        </div>
       
    </div>
@stop