@extends('admin.layout.template')

@section('page-heading', 'Saradnik')

@section('content')
    <div class="panel-heading">

    </div>
    <div class="panel-body">
        <div class="tab-content">
            <div class="tab-pane active" id="form">
                <form class="form-horizontal" action="{{ route('admin.associate.update', $associate->id) }}" data-resource="associate" method="post" enctype="multipart/form-data">                
                    {{ csrf_field() }}
                    {{ method_field('put') }}
                    
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="col-sm-2 control-label">Naziv</label>
                            <div class="col-sm-6">
                                <textarea class="form-control slugify" id="name" name="name">{{ $associate->name }}</textarea>
                            </div>
                            <div class="col-sm-3">
                                <p class="help-block">{{ $errors->first('name') }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-2">  
                                <div class="fileinput fileinput-exists" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 360px; height: 200px;"></div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 360px; max-height: auto;">
                                        <img src="{{ config('settings.image.associates.upload_dir').$associate->filename }}" class="img-responsive" alt="">
                                    </div>
                                    <div>
                                        <span class="btn btn-default btn-file"><span class="fileinput-new">Izaberi sliku</span><span class="fileinput-exists">Izmeni</span>
                                        <input type="file" name="file" accept="image/*"></span>
                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Izbriši</a>
                                    </div>
                                </div>
                                <p class="help-block">{{ $errors->first('file') }}</p>
                                <p>Preporučena dimenzija: <b>visina slike - 40px<b></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4 col-md-offset-3">
                            <input type="hidden" name="is_active" value="{{ $associate->is_active }}">
                            <div class="toggle toggle-success {{ $associate->is_active ? 'active' : '' }} form-active" title="Status" data-id="{{ $associate->id }}"></div>
                        </div>
                        <div class="col-sm-3">
                            <button class="btn-primary btn">Snimi</button>                    
                        </div>
                    </div>
                </form>
            </div>
        </div>
       
    </div>
@stop