@extends('admin.layout.template')

@section('page-heading', 'Specifikacija proizvoda')

@section('content')
    <div class="panel-heading">
        <h4>Nova Specifikacija</h4>
        <div class="options">

        </div>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" action="{{ route('admin.specification.store') }}" data-resource="specification" method="post" enctype="multipart/form-data">                
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="col-sm-2 control-label">Naziv</label>
                            <div class="col-sm-6">
                                <textarea class="form-control" id="name" name="name"></textarea>
                            </div>
                            <div class="col-sm-3">
                                <p class="help-block"></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="product-select" class="col-sm-2 control-label">Proizvod</label>
                            <div class="col-sm-6">
                                <select id="product-select" class="form-control" name="product_id">
                                    <option value="-1" selected >Izaberi proizvod</option>
                                    @if($products)
                                        @foreach($products as $product)
                                            <option value="{{ $product->id }}">{{ $product->title }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">    
                            <label for="text" class="col-sm-2 control-label">Tekst</label>
                            <div class="col-sm-6">
                                <textarea rows="6" class="form-control" id="text" name="text"></textarea>
                            </div>
                            <div class="col-sm-3">
                                <p class="help-block"></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4 col-md-offset-3">
                            <input type="hidden" name="is_active" value="0">
                            <div class="toggle toggle-success form-active"></div>
                        </div>
                        <div class="col-sm-3">
                            <button class="btn-primary btn">Snimi</button>                    
                        </div>
                    </div>
                </div>
                {{--  col-md-8  --}}
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="related_products">Povezani proizvodi:</label>
                        <select class="form-control" id="related_products" name="related_products[]" multiple="multiple">
                            <option value="-1">-- Izaberi --</option>
                            @if($products)
                                @foreach($products as $product)
                                    <option value="{{ $product->id }}" >{{ $product->title }}</option>
                                @endforeach
                            @else
                            @endif
                        </select>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop