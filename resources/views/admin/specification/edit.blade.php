@extends('admin.layout.template')

@section('page-heading', 'Specifikacija')

@section('content')
    <div class="panel-heading">
        <h4>Uredi Specifikaciju</h4>
        <div class="options">

        </div>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" data-resource="specification" action="{{ route('admin.specification.update', $specification->id) }}" method="post" enctype="multipart/form-data">
            <div class="col-md-12" >
                {{ csrf_field() }}
                {{ method_field('put') }}
                <div class="row">
                    <div class="col-md-8">

                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Naziv</label>
                            <div class="col-sm-6">
                                <textarea class="form-control slugify" id="name" name="name">{{ $specification->name }}</textarea>
                            </div>
                            <div class="col-sm-3">
                                <p class="help-block"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">    
                                <label for="text" class="col-sm-2 control-label">Tekst</label>
                                <div class="col-sm-6">
                                    <textarea rows="6" class="form-control" id="text" name="text">{{ $specification->text }}</textarea>
                                </div>
                                <div class="col-sm-3">
                                    <p class="help-block"></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label for="product-select" class="col-sm-2 control-label">Proizvod</label>
                                <div class="col-sm-6">
                                    <select id="product-select" class="form-control" name="product_id">
                                        
                                        @if($products)
                                        @foreach($products as $product)
                                        <option {{ $specification->product_id == $product->id ? 'selected' : '' }} value="{{ $product->id }}">{{ $product->title }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <input type="hidden" name="is_active" value="{{ $specification->is_active }}">
                                <div class="toggle toggle-success {{ $specification->is_active ? 'active' : '' }} form-active" title="Status" data-id="{{ $specification->id }}"></div>
                            </div>
                            <div class="col-sm-3">
                                <button class="btn-primary btn">Snimi</button>                    
                            </div>
                        </div>
                    </div>
                    {{--  col-md-8  --}}
                    <div class="col-md-2">
                        {{--  @if($relatedProducts)  --}}
                        <div class="form-group">
                            <label for="related_products">Povezani proizvodi:</label>
                            <select class="form-control" id="related_products" name="related_products[]" multiple="multiple">
                                <option value="-1">-- Izaberi --</option>
                                @if($products)
                                    @foreach($products as $key => $product)
                                        <option value="{{ $product->id }}" {{ isset($relatedProducts) ? in_array($product->id, $relatedProducts) ? 'selected="selected"' : '' : '' }}>{{ $product->title }}</option>
                                    @endforeach
                                @else
                                @endif
                            </select>
                        </div>
                        {{--  @endif  --}}
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop