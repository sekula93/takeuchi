@extends('admin.layout.template')

@section('page-heading', 'Specifikacije proizvoda')

@section('content')
    @include('flash::message')
    <div class="panel-heading">
        <h4>Specifikacije</h4>
        <div class="options">

        </div>
    </div>
    <div class="panel-body resource-container" data-resource="specification" data-modal="Da li želiš da obrišeš specifikaciju?">
        @if(count($listArray))
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Naziv</th>
                    <th>Kategorija</th>
                    <th>Aktivan</th>
                    <th>Opcije</th>
                </tr>
            </thead>
            <tbody>
                    @foreach($listArray as $key => $item)
                    <tr data-id="{{ $item->id }}" id="item-{{ $item->id }}">
                        <td>{{ $key+1 }}</td>
                        <td><a href="{{ route('admin.specification.edit', $item->id) }}">{{ $item->name }}</a></td>
                        <td>{{ $item->product->title }}</td>
                        <td class="table-active">
                            <div class="toggle toggle-success {{ $item->is_active ? 'active' : '' }} table-ajax" title="Status" data-id="{{ $item->id }}"></div>
                        </td>
                        <td class="table-options">
                            <a href="{{ route('admin.specification.edit', $item->id) }}" class="option-edit"><i class="fa fa-pencil"></i></a>
                            <a style="margin-left: 15px; cursor: pointer; color: red;" class="delete-item"><i class="fa fa-times"></i> </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <h3>Lista je trenutno prazna</h3>
        @endif
        </div>
@stop