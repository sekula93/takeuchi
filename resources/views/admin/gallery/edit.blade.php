@extends('admin.layout.template')

@section('page-heading', 'Galerija')

@section('content')
    <div class="panel-heading">
        
        {{--  <h4>Uredi gallery</h4>  --}}
        <div class="options">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#form" data-toggle="tab">Forma</a>
                </li>
                <li>
                    <a href="#packshots" data-toggle="tab">Fotografije</a>
                </li>
            </ul>    
        </div>
    </div>
    <div class="panel-body">
        <div class="tab-content">
            <div class="tab-pane active" id="form">
                <form class="form-horizontal" action="{{ route('admin.gallery.update', $gallery->id) }}" data-resource="gallery" method="post" enctype="multipart/form-data">                
                        {{ csrf_field() }}
                        {{ method_field('put') }}
        
                        <div class="form-group">
                            <div class="row">
                                <label for="title" class="col-sm-2 control-label">Naziv</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control slugify" id="title" name="title">{{ $gallery->title }}</textarea>
                                </div>
                                <div class="col-sm-3">
                                    <p class="help-block"></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label for="slug" class="col-sm-2 control-label">Slug</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="slug" name="slug" value="{{ $gallery->slug }}">
                                </div>
                                <div class="col-sm-3">
                                    <p class="help-block"></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">    
                                <label for="excerpt" class="col-sm-2 control-label">Uvod</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control" id="excerpt" name="excerpt">{{ $gallery->excerpt }}</textarea>
                                </div>
                                <div class="col-sm-3">
                                    <p class="help-block"></p>
                                </div>
                            </div>
                        </div>
                        {{--  <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3 col-sm-offset-2">  
                                    <div class="fileinput fileinput-exists" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 360px; height: 200px;"></div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 360px; max-height: auto;">
                                            <img src="{{ config('settings.image.gallery.upload_dir').$gallery->filename }}" class="img-responsive" alt="">
                                        </div>
                                        <div>
                                            <span class="btn btn-default btn-file"><span class="fileinput-new">Izaberi sliku</span><span class="fileinput-exists">Izmeni</span>
                                            <input type="file" name="file" accept="image/*"></span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Izbriši</a>
                                        </div>
                                    </div>
                                </div>
                                <div class"col-sm-3">
                                    <label for="gallery_date" class="control-label">Datum objave:</label>
                                    <input type="date" id="gallery_date" name="gallery_date" value="{{ date('Y-m-d', strtotime($gallery->created_at)) }}">
                                </div>
                            </div>
                        </div>  --}}
                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-3">
                                <input type="hidden" name="is_active" value="{{ $gallery->is_active }}">
                                <div class="toggle toggle-success {{ $gallery->is_active ? 'active' : '' }} form-active" title="Status" data-id="{{ $gallery->id }}"></div>
                            </div>
                            <div class="col-sm-3">
                                <button class="btn-primary btn">Snimi</button>                    
                            </div>
                        </div>
                </form>
            </div>
            <div class="tab-pane" id="packshots">
                {{--  <form action="#" class="fileupload-photo" >  --}}
                    <form class="fileupload-photo" action="{{ route('admin.gallery-photo.store', $gallery->id) }}" method="POST" enctype="multipart/form-data">
                        <span class="btn btn-success fileinput-button photo-upload-button">
                            <h2><i class="fa fa-cloud-upload"></i>Spustite fajlove ovde</h2>
                            <h4>ili kliknite</h4>
                            <!-- The file input field used as target for the file upload widget -->
                            <input type="file" name="files[]" multiple>
                        </span>
                    </form>
                    <br>
                    <br>
                    <!-- The global progress bar -->
                    <div class="progress-bar progress-bar-success col-md-12" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100" style="">
                        <span class="sr-only"></span>
                    </div>
                    <div class="col-md-12 errors-wrapper"></div>
            
                    <ul class="gallery images gallery uploaded list-unstyled col-sm-12" data-type="1">
                        @if($gallery->photos)
                        @foreach ($gallery->photos as $photo)
                        <li class="image" data-resource="gallery-photo" data-name="" data-id="{{ $photo->id }}" id="file-{{ $photo->id }}">
                            <a class="file-thumb {{ !$photo['is_active'] ? 'inactive' : ''}}" href="/uploads/gallery/images/{{ $photo->filename }}" data-lightbox="photos">
                                <img src="/uploads/gallery/images/{{ $photo->filename }}" width="230"></a>
                            </a>
                            <div class="toggle toggle-success {{ $photo->is_active ? 'active' : '' }} file-active" title="Status"></div>
                            <button type="button"  class="btn btn-danger-alt remove-file"><i class="fa fa-times"></i></button>
                        </li>
                        @endforeach
                        @endif
                    </ul>
                
                {{--  </form>  --}}
            </div>
        </div>
       
    </div>
@stop