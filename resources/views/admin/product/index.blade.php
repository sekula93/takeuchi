@extends('admin.layout.template')

@section('page-heading', 'Proizvodi')

@section('content')
    @include('flash::message')
    <div class="panel-heading">
        <div class="options">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#products" data-toggle="tab">Proizvodi</a>
                </li>
                <li>
                    <a href="#rental" data-toggle="tab">Rental</a>
                </li>
                <li>
                    <a href="#used" data-toggle="tab">Polovne mašine</a>
                </li>
            </ul>    
        </div>
    </div>
    <div class="panel-body resource-container" data-resource="product" data-modal="Da li želiš da obrišeš proizvod?">
        <div class="tab-content">
            <div class="tab-pane active" id="products">
                @if(count($listArray))
                <table class="table sortable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Naslov</th>
                            <th>Podkategorija</th>
                            <th class="text-center">Aktivan</th>
                            <th>Opcije</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($listArray as $key => $item)
                        @if($item->type == 1)
                        <tr data-id="{{ $item->id }}" id="item-{{ $item->id }}">
                            <td style="cursor: move;" class="bars"><i class="fa fa-bars"></i></td>
                            <td><a href="{{ route('admin.product.edit', $item->id) }}">{{ $item->title }}</a></td>
                            <td>{{ $item->productSubcategory->name }}</td>
                            <td class="table-active">
                                <div class="toggle toggle-success {{ $item->is_active ? 'active' : '' }} table-ajax" title="Status" data-id="{{ $item->id }}"></div>
                            </td>
                            <td class="table-options">
                                <a href="{{ route('admin.product.edit', $item->id) }}" class="option-edit"><i class="fa fa-pencil"></i></a>
                                <a style="margin-left: 15px; cursor: pointer; color: red;" class="delete-item"><i class="fa fa-times"></i> </a>
                            </td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
                @else
                <h3>Lista je trenutno prazna</h3>
                @endif
            </div>
            <div class="tab-pane" id="rental">
                @if(count($listArray))
                <table class="table sortable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Naslov</th>
                            <th>Podkategorija</th>
                            <th class="text-center">Aktivan</th>
                            <th>Opcije</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($listArray as $key => $item)
                        @if($item->type == 2)
                        <tr data-id="{{ $item->id }}" id="item-{{ $item->id }}">
                            <td style="cursor: move;" class="bars"><i class="fa fa-bars"></i></td>
                            <td><a href="{{ route('admin.product.edit', $item->id) }}">{{ $item->title }}</a></td>
                            <td>{{ $item->productSubcategory->name }}</td>
                            <td class="table-active">
                                <div class="toggle toggle-success {{ $item->is_active ? 'active' : '' }} table-ajax" title="Status" data-id="{{ $item->id }}"></div>
                            </td>
                            <td class="table-options">
                                <a href="{{ route('admin.product.edit', $item->id) }}" class="option-edit"><i class="fa fa-pencil"></i></a>
                                <a style="margin-left: 15px; cursor: pointer; color: red;" class="delete-item"><i class="fa fa-times"></i> </a>
                            </td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
                @else
                <h3>Lista je trenutno prazna</h3>
                @endif
            </div>
            <div class="tab-pane" id="used">
                    @if(count($listArray))
                    <table class="table sortable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Naslov</th>
                                <th>Podkategorija</th>
                                <th class="text-center">Aktivan</th>
                                <th>Opcije</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($listArray as $key => $item)
                            @if($item->type == 3)
                            <tr data-id="{{ $item->id }}" id="item-{{ $item->id }}">
                                <td style="cursor: move;" class="bars"><i class="fa fa-bars"></i></td>
                                <td><a href="{{ route('admin.product.edit', $item->id) }}">{{ $item->title }}</a></td>
                                <td>{{ $item->productSubcategory->name }}</td>
                                <td class="table-active">
                                    <div class="toggle toggle-success {{ $item->is_active ? 'active' : '' }} table-ajax" title="Status" data-id="{{ $item->id }}"></div>
                                </td>
                                <td class="table-options">
                                    <a href="{{ route('admin.product.edit', $item->id) }}" class="option-edit"><i class="fa fa-pencil"></i></a>
                                    <a style="margin-left: 15px; cursor: pointer; color: red;" class="delete-item"><i class="fa fa-times"></i> </a>
                                </td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <h3>Lista je trenutno prazna</h3>
                    @endif
            </div>
        </div>
    </div>
@stop