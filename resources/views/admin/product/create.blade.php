@extends('admin.layout.template')

@section('page-heading', 'Proizvod')

@section('content')
    <div class="panel-heading">
        <h4>Novi proizvod</h4>
        <div class="options">

        </div>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" action="{{ route('admin.product.store') }}" data-resource="product" method="post" enctype="multipart/form-data">                
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-sm-2 control-label">Naslov</label>
                        <div class="col-sm-6">
                            <textarea class="form-control slugify" id="title" name="title"></textarea>
                        </div>
                        <div class="col-sm-3">
                            <p class="help-block">{{ $errors->first('title') }}</p>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="slug" class="col-sm-2 control-label">Slug</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="slug" name="slug">
                        </div>
                        <div class="col-sm-3">
                            <p class="help-block">{{ $errors->first('slug') }}</p>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="subcategory-select" class="col-sm-2 control-label">Podkategorija</label>
                        <div class="col-sm-6">
                            <select id="subcategory-select" class="form-control" name="subcategory_id">
                                <option value="-1" selected >Izaberi podkategoriju</option>
                                @if($categories)
                                    @foreach($categories as $subcategory)
                                        <option value="{{ $subcategory->id }}">{{ $subcategory->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <p class="help-block">{{ $errors->first('subcategory_id') }}</p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="type" class="col-sm-2 control-label"></label>
                        <div class="col-sm-6">
                            <select id="type" class="form-control" name="type">
                                <option value="1">Proizvodi</option>
                                <option value="2">Rental</option>
                                <option value="3">Polovne mašine</option>
                            </select>
                        </div>
                        <p class="help-block">{{ $errors->first('subcategory_id') }}</p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">    
                        <label for="text" class="col-sm-2 control-label">Tekst</label>
                        <div class="col-sm-6">
                            <textarea rows="6" class="form-control" id="text" name="text"></textarea>
                        </div>
                        <div class="col-sm-3">
                            <p class="help-block">{{ $errors->first('text') }}</p>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3 col-sm-offset-2">  
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 360px; height: 200px;"></div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 360px; max-height: auto;"></div>
                                <div>
                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Izaberi sliku</span><span class="fileinput-exists">Izmeni</span>
                                    <input type="file" name="file" accept="image/*"></span>
                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Izbriši</a>
                                </div>
                            </div>
                            <p class="help-block">{{ $errors->first('file') }}</p>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3 col-sm-offset-2">
                            <label for="attachment_label">Naziv dokumenta</label>
                            <input type="text" name="attachment_label" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3 col-sm-offset-2">
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput">
                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                    <span class="fileinput-filename"></span>
                                </div>
                                <span class="input-group-addon btn btn-default btn-file">
                                    <span class="fileinput-new">Izaberi dokument</span>
                                    <span class="fileinput-exists">Izmeni</span>
                                    <input type="file" name="attachment" accept="application/pdf">
                                </span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Obriši</a>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4 col-md-offset-3">
                        <input type="hidden" name="is_active" value="0">
                        <div class="toggle toggle-success form-active"></div>
                        <p class="help-block">{{ $errors->first('is_active') }}</p>
                    </div>
                    <div class="col-sm-3">
                        <button class="btn-primary btn">Snimi</button>                    
                    </div>
                </div>
        </form>
    </div>
@stop