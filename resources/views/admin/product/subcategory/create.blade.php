@extends('admin.layout.template')

@section('page-heading', 'Kategorija proizvoda')

@section('content')
    <div class="panel-heading">
        <h4>Nova podkategorija</h4>
        <div class="options">

        </div>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" action="{{ route('admin.product-subcategory.store') }}" data-resource="product-subcategory" method="post" enctype="multipart/form-data">                
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="row">
                        <label for="name" class="col-sm-2 control-label">Naslov</label>
                        <div class="col-sm-6">
                            <textarea class="form-control slugify" id="name" name="name"></textarea>
                        </div>
                        <div class="col-sm-3">
                            <p class="help-block">{{ $errors->first('name') }}</p>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="slug" class="col-sm-2 control-label">Slug</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="slug" name="slug">
                        </div>
                        <div class="col-sm-3">
                            <p class="help-block">{{ $errors->first('slug') }}</p>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="category-select" class="col-sm-2 control-label">Kategorija</label>
                        <div class="col-sm-6">
                            <select id="category-select" class="form-control" name="category_id">
                                <option value="-1" selected >Izaberi kategoriju</option>
                                @if($categories)
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                            <div class="checkbox block">
                                <label><input type="checkbox" name="checkbox"> Bez proizvoda</label>
                            </div>
                        </div>
                    </div>
                </div>
                {{--  <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3 col-sm-offset-2">  
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 360px; height: 200px;"></div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 360px; max-height: auto;"></div>
                                <div>
                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Izaberi sliku</span><span class="fileinput-exists">Izmeni</span>
                                    <input type="file" name="file" accept="image/*"></span>
                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Izbriši</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  --}}
                <div class="form-group">
                    <div class="col-md-4 col-md-offset-3">
                        <input type="hidden" name="is_active" value="0">
                        <div class="toggle toggle-success form-active"></div>
                    </div>
                    <div class="col-sm-3">
                        <button class="btn-primary btn">Snimi</button>                    
                    </div>
                </div>
        </form>
    </div>
@stop