@extends('admin.layout.template')

@section('page-heading', 'Kategorija')

@section('content')
    <div class="panel-heading">
        <h4>Uredi Subkategoriju</h4>
        <div class="options">

        </div>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" data-resource="product-subcategory" action="{{ route('admin.product-subcategory.update', $subcategory->id) }}" method="post" enctype="multipart/form-data">
            <div class="col-md-6" >
                {{ csrf_field() }}
                {{ method_field('put') }}

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Naziv</label>
                    <div class="col-sm-6">
                        <textarea class="form-control slugify" id="name" name="name">{{ $subcategory->name }}</textarea>
                    </div>
                    <div class="col-sm-3">
                        <p class="help-block">{{ $errors->first('name') }}</p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="slug" class="col-sm-2 control-label">Slug</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="slug" name="slug" value="{{ $subcategory->slug }}">
                        </div>
                        <div class="col-sm-3">
                            <p class="help-block">{{ $errors->first('slug') }}</p>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                            <div class="checkbox block">
                                <label><input type="checkbox" name="checkbox" {{ $subcategory->flag ? 'checked' : '' }}> Bez proizvoda</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                {{--  <div class="form-group">
                    <div class="col-sm-9">  
                        <div class="fileinput fileinput-exists" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 360px; height: 240px;"></div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 360px; max-height:auto;">
                                    <img src="{{ config('settings.image.product-subcategory.upload_dir').$subcategory->filename }}" class="img-responsive" alt="">
                                </div>
                                <div>
                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Izaberi sliku</span><span class="fileinput-exists">Izmeni</span>
                                    <input type="file" name="file" accept="image/*"></span>
                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Izbriši</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  --}}
                <div class="form-group">
                    <div class="row">
                        <label for="category-select" class="col-sm-2 control-label">Kategorija</label>
                        <div class="col-sm-6">
                            <select id="category-select" class="form-control" name="category_id">
                                @if($categories)
                                    @foreach($categories as $category)
                                        {{--  <option {{ $subcategory->category_id == $category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>  --}}
                                        <option {{ $category->id == $subcategory->product_category_id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <input type="hidden" name="is_active" value="{{ $subcategory->is_active }}">
                        <div class="toggle toggle-success {{ $subcategory->is_active ? 'active' : '' }} form-active" title="Status" data-id="{{ $subcategory->id }}"></div>
                    </div>
                    <div class="col-sm-3">
                        <button class="btn-primary btn">Snimi</button>                    
                    </div>
                </div>
            </form>
    </div>
@stop