@extends('admin.layout.template')

@section('page-heading', 'Proizvodi')

@section('content')
    <div class="panel-heading">
        
        {{--  <h4>Uredi product</h4>  --}}
        <div class="options">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#form" data-toggle="tab">Forma</a>
                </li>
                <li>
                    <a href="#packshots" data-toggle="tab">Fotografije</a>
                </li>
            </ul>    
        </div>
    </div>
    <div class="panel-body">
        <div class="tab-content">
            <div class="tab-pane active" id="form">
                <form class="form-horizontal" action="{{ route('admin.product.update', $product->id) }}" data-resource="product" method="post" enctype="multipart/form-data">                
                        {{ csrf_field() }}
                        {{ method_field('put') }}
        
                        <div class="form-group">
                            <div class="row">
                                <label for="title" class="col-sm-2 control-label">Naslov</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control slugify" id="title" name="title">{{ $product->title }}</textarea>
                                </div>
                                <div class="col-sm-3">
                                    <p class="help-block">{{ $errors->first('title') }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label for="slug" class="col-sm-2 control-label">Slug</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="slug" name="slug" value="{{ $product->slug }}">
                                </div>
                                <div class="col-sm-3">
                                    <p class="help-block">{{ $errors->first('slug') }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label for="category-select" class="col-sm-2 control-label">Podkategorija</label>
                                <div class="col-sm-6">
                                    <select id="category-select" class="form-control" name="subcategory_id">
                                        @if($subcategories)
                                            @foreach($subcategories as $category)
                                                <option {{ $product->product_subcategory_id == $category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <p class="help-block">{{ $errors->first('subcategory_id') }}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label for="type" class="col-sm-2 control-label"></label>
                                <div class="col-sm-6">
                                    <select id="type" class="form-control" name="type">
                                        <option value="1" {{ $product->type == 1 ? 'selected' : '' }} >Proizvodi</option>
                                        <option value="2" {{ $product->type == 2 ? 'selected' : '' }} >Rental</option>
                                        <option value="3" {{ $product->type == 3 ? 'selected' : '' }} >Polovne mašine</option>
                                    </select>
                                </div>
                                <p class="help-block">{{ $errors->first('subcategory_id') }}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">    
                                <label for="text" class="col-sm-2 control-label">Tekst</label>
                                <div class="col-sm-6">
                                    <textarea rows="6" class="form-control" id="text" name="text">{{ $product->text }}</textarea>
                                </div>
                                <div class="col-sm-3">
                                    <p class="help-block">{{ $errors->first('text') }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3 col-sm-offset-2">  
                                    <div class="fileinput fileinput-exists" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 360px; height: 200px;"></div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 360px; max-height: auto;">
                                            <img src="{{ config('settings.image.product.upload_dir').$product->filename }}" class="img-responsive" alt="">
                                        </div>
                                        <div>
                                            <span class="btn btn-default btn-file"><span class="fileinput-new">Izaberi sliku</span><span class="fileinput-exists">Izmeni</span>
                                            <input type="file" name="file" accept="image/*"></span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Izbriši</a>
                                        </div>
                                    </div>
                                    <p class="help-block">{{ $errors->first('file') }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3 col-sm-offset-2">
                                    <label for="attachment_label">Naziv dokumenta</label>
                                    <input type="text" name="attachment_label" class="form-control" value="{{ $product->attachment_label }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3 col-sm-offset-2">
                                    <div class="fileinput fileinput-exists input-group" data-provides="fileinput">
                                        <div class="form-control" data-trigger="fileinput">
                                            <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                            <span class="fileinput-filename">{{ $product->attachment_label }}</span>
                                        </div>
                                        <span class="input-group-addon btn btn-default btn-file">
                                            <span class="fileinput-new">Izaberi dokument</span>
                                            <span class="fileinput-exists">Izmeni</span>
                                            <input type="file" name="attachment" accept="application/pdf">
                                        </span>
                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Obriši</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-3">
                                <input type="hidden" name="is_active" value="{{ $product->is_active }}">
                                <div class="toggle toggle-success {{ $product->is_active ? 'active' : '' }} form-active" title="Status" data-id="{{ $product->id }}"></div>
                            </div>
                            <p class="help-block">{{ $errors->first('is_active') }}</p>
                            <div class="col-sm-3">
                                <button class="btn-primary btn">Snimi</button>                    
                            </div>
                        </div>
                </form>
            </div>
            <div class="tab-pane" id="packshots">
                {{--  <form action="#" class="fileupload-photo" >  --}}
                    <form class="fileupload-photo" action="{{ route('admin.product-photo.store', $product->id) }}" method="post" enctype="multipart/form-data">
                        <span class="btn btn-success fileinput-button photo-upload-button">
                            <h2><i class="fa fa-cloud-upload"></i>Spustite fajlove ovde</h2>
                            <h4>ili kliknite</h4>
                            <!-- The file input field used as target for the file upload widget -->
                            <input type="file" name="files[]" multiple>
                        </span>
                    </form>
                    <br>
                    <br>
                    <!-- The global progress bar -->
                    <div class="progress-bar progress-bar-success col-md-12" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100" style="">
                        <span class="sr-only"></span>
                    </div>
                    <div class="col-md-12 errors-wrapper"></div>
            
                    <ul class="product images gallery uploaded list-unstyled col-sm-12" data-type="1">
                        @if($product->photos)
                        @foreach ($product->photos as $photo)
                        <li class="image" data-resource="product-photo" data-name="" data-id="{{ $photo->id }}" id="file-{{ $photo->id }}">
                            <a class="file-thumb {{ !$photo['is_active'] ? 'inactive' : ''}}" href="/uploads/products/images/{{ $photo->filename }}" data-lightbox="photos">
                                <img src="/uploads/products/images/{{ $photo->filename }}" width="230"></a>
                            </a>
                            <div class="toggle toggle-success {{ $photo->is_active ? 'active' : '' }} file-active" title="Status"></div>
                            <button type="button"  class="btn btn-danger-alt remove-file"><i class="fa fa-times"></i></button>
                        </li>
                        @endforeach
                        @endif
                    </ul>
                {{--  </form>  --}}
            </div>
        </div>
       
    </div>
@stop