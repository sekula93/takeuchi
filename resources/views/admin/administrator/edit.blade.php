@extends('admin.layout.template')

@section('page-heading', 'Administrator')

@section('content')
    <div class="panel-heading">
        <h4>Uredi informacije</h4>
        <div class="options">

        </div>
    </div>
    <div class="panel-body">
        <div class="col-md-6" >
            <form class="form-horizontal" action="{{ route('admin.administrator.update', $user->id) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('put') }}
                <div class="form-group">
                    <label for="name" class="col-sm-3 control-label">Ime i prezime</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}">
                    </div>
                    <div class="col-sm-3">
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-6">
                        <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}">
                    </div>
                    <div class="col-sm-3">
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-3 control-label">Lozinka</label>
                    <div class="col-sm-6">
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                    <div class="col-sm-3">
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password_confirmation" class="col-sm-3 control-label">Potvrdi lozinku</label>
                    <div class="col-sm-6">
                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
                    </div>
                    <div class="col-sm-3">
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="form-group">
                </div>
                <div class="form-group">
                    <div class="col-md-4 col-md-offset-3">
                        <input type="hidden" name="is_active" value="{{ $user->is_active ? 1 : 0 }}">
                        <div class="toggle toggle-success {{ $user->is_active ? 'active' : '' }} form-active" title="Status" data-id="{{ $user->id }}"></div>
                    </div>
                    <div class="col-sm-3">
                        <button class="btn-primary btn">Snimi</button>                    
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop