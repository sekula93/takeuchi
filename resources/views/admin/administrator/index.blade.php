@extends('admin.layout.template')

@section('page-heading', 'Dashboard')

@section('content')
    @include('flash::message')
    <div class="panel-heading">
        <h4>Dashboard</h4>
        <div class="options">

        </div>
    </div>
    <div class="panel-body resource-container" data-resource="administrator" data-modal="Da li želiš da obrišeš administratora?">
        @if(count($listArray))
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Ime</th>
                    <th>Email</th>
                    <th>Aktivan</th>
                    <th>Opcije</th>
                </tr>
            </thead>
            <tbody>
                    @foreach($listArray as $key => $item)
                    <tr data-id="{{ $item->id }}">
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->email }}</td>
                        <td class="table-active">
                            <div class="toggle toggle-success {{ $item->is_active ? 'active' : '' }} table-ajax" title="Status" data-id="{{ $item->id }}"></div>
                        </td>
                        <td class="table-options">
                            <a href="{{ route('admin.administrator.edit', $item->id) }}" class="option-edit"><i class="fa fa-pencil"></i></a>
                            <a style="margin-left: 15px; cursor: pointer; color: red;" class="delete-item"><i class="fa fa-times"></i> </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <h3>Lista je trenutno prazna</h3>
        @endif
        </div>
@stop