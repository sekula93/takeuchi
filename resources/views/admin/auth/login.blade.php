<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Avant</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Avant">
    <meta name="author" content="The Red Team">

    <!-- <link href="/assets/admin/less/styles.less" rel="stylesheet/less" media="all"> -->
    <link rel="stylesheet" href="/assets/admin/css/styles.css?=140">
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600' rel='stylesheet' type='text/css'>
    
</head><body class="focusedform">

<div class="verticalcenter">
	{{--  <a href="index.htm"><img src="/assets/admin/img/logo-big.png" alt="Logo" class="brand" /></a>  --}}
	<div class="panel panel-primary">
		<form action="{{ route('admin.auth.post-login') }}" method="post" class="form-horizontal" style="margin-bottom: 0px !important;">
			{{ csrf_field() }}
			<div class="panel-body">
			<h4 class="text-center" style="margin-bottom: 25px;">Administrator</h4>
				<div class="form-group">
					<div class="col-sm-12">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-user"></i></span>
							<input type="email" class="form-control" name="email" id="email" placeholder="Email adresa">
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-lock"></i></span>
							<input type="password" class="form-control" name="password" id="password" placeholder="Lozinka">
						</div>
					</div>
				</div>
				
			</div>
			<div class="panel-footer">
				{{--  <a href="extras-forgotpassword.htm" class="pull-left btn btn-link" style="padding-left:0">Forgot password?</a>  --}}
				
				<div class="pull-right">
					{{--  <a href="#" class="btn btn-default">Reset</a>  --}}
					<button class="btn btn-primary">Log In</button>
				</div>
			</div>
			</div>
		</form>
 </div>
      
</body>
</html>