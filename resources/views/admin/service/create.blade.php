@extends('admin.layout.template')

@section('page-heading', 'Saradnik')

@section('content')
    <div class="panel-heading">
        <h4>Nova usluga</h4>
        <div class="options">

        </div>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" action="{{ route('admin.service.store') }}" data-resource="service" method="post" enctype="multipart/form-data">                
            {{ csrf_field() }}
            <div class="form-group">
                <div class="row">
                    <label for="title" class="col-sm-2 control-label">Naslov</label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="title" name="title">{{ old('title') }}</textarea>
                    </div>
                    <div class="col-sm-3">
                        <p class="help-block">{{ $errors->first('title') }}</p>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label for="subtitle" class="col-sm-2 control-label">Tekst</label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="subtitle" name="subtitle">{{ old('subtitle') }}</textarea>
                    </div>
                    <div class="col-sm-3">
                        <p class="help-block">{{ $errors->first('subtitle') }}</p>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-4 col-md-offset-3">
                    <input type="hidden" name="is_active" value="0">
                    <div class="toggle toggle-success form-active"></div>
                </div>
                <div class="col-sm-3">
                    <button class="btn-primary btn">Snimi</button>                    
                </div>
            </div>
        </form>
    </div>
@stop