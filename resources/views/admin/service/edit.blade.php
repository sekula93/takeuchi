@extends('admin.layout.template')

@section('page-heading', 'Usluga')

@section('content')
    <div class="panel-heading">

    </div>
    <div class="panel-body">
        <div class="tab-content">
            <div class="tab-pane active" id="form">
                <form class="form-horizontal" action="{{ route('admin.service.update', $service->id) }}" data-resource="service" method="post" enctype="multipart/form-data">                
                    {{ csrf_field() }}
                    {{ method_field('put') }}
                    
                    <div class="form-group">
                        <div class="row">
                            <label for="title" class="col-sm-2 control-label">Naslov</label>
                            <div class="col-sm-6">
                                <textarea class="form-control" id="title" name="title">{{ $service->title }}</textarea>
                            </div>
                            <div class="col-sm-3">
                                <p class="help-block">{{ $errors->first('title') }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                            <div class="row">
                                <label for="subtitle" class="col-sm-2 control-label">Naziv</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control" id="subtitle" name="subtitle">{{ $service->subtitle }}</textarea>
                                </div>
                                <div class="col-sm-3">
                                    <p class="help-block">{{ $errors->first('subtitle') }}</p>
                                </div>
                            </div>
                        </div>
                    <div class="form-group">
                        <div class="col-md-4 col-md-offset-3">
                            <input type="hidden" name="is_active" value="{{ $service->is_active }}">
                            <div class="toggle toggle-success {{ $service->is_active ? 'active' : '' }} form-active" title="Status" data-id="{{ $service->id }}"></div>
                        </div>
                        <div class="col-sm-3">
                            <button class="btn-primary btn">Snimi</button>                    
                        </div>
                    </div>
                </form>
            </div>
        </div>
       
    </div>
@stop