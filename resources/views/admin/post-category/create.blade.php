@extends('admin.layout.template')

@section('page-heading', 'Kategorija proizvoda')

@section('content')
    <div class="panel-heading">
        <h4>Nova kategorija</h4>
        <div class="options">

        </div>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" action="{{ route('admin.post-category.store') }}" data-resource="post-category" method="post" enctype="multipart/form-data">                
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="row">
                        <label for="name" class="col-sm-2 control-label">Naslov</label>
                        <div class="col-sm-6">
                            <textarea class="form-control slugify" id="name" name="name"></textarea>
                        </div>
                        <div class="col-sm-3">
                            <p class="help-block"></p>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="slug" class="col-sm-2 control-label">Slug</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="slug" name="slug">
                        </div>
                        <div class="col-sm-3">
                            <p class="help-block"></p>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4 col-md-offset-3">
                        <input type="hidden" name="is_active" value="0">
                        <div class="toggle toggle-success form-active"></div>
                    </div>
                    <div class="col-sm-3">
                        <button class="btn-primary btn">Snimi</button>                    
                    </div>
                </div>
        </form>
    </div>
@stop