@extends('admin.layout.template')

@section('page-heading', 'Kategorija')

@section('content')
    <div class="panel-heading">
        <h4>Uredi Kategoriju</h4>
        <div class="options">

        </div>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" data-resource="post-category" action="{{ route('admin.post-category.update', $category->id) }}" method="post" enctype="multipart/form-data">
            <div class="col-md-6" >
                {{ csrf_field() }}
                {{ method_field('put') }}

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Naziv</label>
                    <div class="col-sm-6">
                        <textarea class="form-control slugify" id="name" name="name">{{ $category->name }}</textarea>
                    </div>
                    <div class="col-sm-3">
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="slug" class="col-sm-2 control-label">Slug</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="slug" name="slug" value="{{ $category->slug }}">
                        </div>
                        <div class="col-sm-3">
                            <p class="help-block"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="col-md-4">
                        <input type="hidden" name="is_active" value="0">
                        <div class="toggle toggle-success {{ $category->is_active ? 'active' : '' }} form-active" title="Status" data-id="{{ $category->id }}"></div>
                    </div>
                    <div class="col-sm-3">
                        <button class="btn-primary btn">Snimi</button>                    
                    </div>
                </div>
            </form>
    </div>
@stop