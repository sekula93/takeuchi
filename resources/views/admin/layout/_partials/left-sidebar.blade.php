 <!-- BEGIN SIDEBAR -->
<nav id="page-leftbar" role="navigation">
    <!-- BEGIN SIDEBAR MENU -->
<ul class="acc-menu" id="sidebar">
    {{--  <li id="search">
        <a href="javascript:;"><i class="fa fa-search opacity-control"></i></a>
         <form>
            <input type="text" class="search-query" placeholder="Search...">
            <button type="submit"><i class="fa fa-search"></i></button>
        </form>
    </li>  --}}
    <li class="divider"></li>
    <li>
        <a href="{{ route('admin.dashboard.index') }}"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
    <li class="divider"></li>
    <li>
        <a href="javascript:;"><i class="fa fa-user"></i> <span>Administrator</span></a>
        <ul class="acc-menu">
            <li><a href="{{ route('admin.administrator.index') }}">Administrator list</a></li>
            <li><a href="{{ route('admin.administrator.create') }}">Dodaj administratora</a></li>
        </ul>
    </li>
    <li class="divider"></li>
    <li>
        <a href="javascript:;"><i class="fa fa-image"></i> <span>Slider</span></a>
        <ul class="acc-menu">
            <li><a href="{{ route('admin.slider.index') }}">Slider list</a></li>
            <li><a href="{{ route('admin.slider.create') }}">Dodaj slider</a></li>
        </ul>
    </li>
    <li>
        <a href="javascript:;"><i class="fa fa-briefcase"></i> <span>Usluge</span></a>
        <ul class="acc-menu">
            <li><a href="{{ route('admin.service.index') }}">Lista usluga</a></li>
            <li><a href="{{ route('admin.service.create') }}">Dodaj uslugu</a></li>
        </ul>
    </li>
    <li>
        <a href="javascript:;"><i class="fa fa-briefcase"></i> <span>Saradnici</span></a>
        <ul class="acc-menu">
            <li><a href="{{ route('admin.associate.index') }}">Lista saradnika</a></li>
            <li><a href="{{ route('admin.associate.create') }}">Dodaj saradnika</a></li>
        </ul>
    </li>
    <li>
        <a href="javascript:;"><i class="fa fa-file-word-o"></i> <span>Vesti</span></a>
        <ul class="acc-menu">
            <li><a href="{{ route('admin.post.index') }}">Lista vesti</a></li>
            <li><a href="{{ route('admin.post.create') }}">Dodaj vest</a></li>
            <li><a href="javascript:;"></i><span>Kategorije</span></a>
                <ul class="acc-menu">
                    <li><a href="{{ route('admin.post-category.create') }}">Dodaj kategoriju</a></li>
                    <li><a href="{{ route('admin.post-category.index') }}">Lista kategorija</a></li>
                </ul>
            </li>
        </ul>
    </li>
    <li class="divider"></li>
    <li>
        <a href="javascript:;"><i class="fa fa-building"></i> <span>Proizvod</span></a>
        <ul class="acc-menu">
            <li><a href="javascript:;"></i><span>Kategorije proizvoda</span></a>
                <ul class="acc-menu">
                    <li><a href="{{ route('admin.product-category.create') }}">Dodaj kategoriju</a></li>
                    <li><a href="{{ route('admin.product-category.index') }}">Lista kategorija</a></li>
                </ul>
            </li>
            <li><a href="javascript:;"></i><span>Podkategorije proizvoda</span></a>
                <ul class="acc-menu">
                    <li><a href="{{ route('admin.product-subcategory.create') }}">Dodaj podkategoriju</a></li>
                    <li><a href="{{ route('admin.product-subcategory.index') }}">Lista podkategorija</a></li>
                </ul>
            </li>
            <li><a href="{{ route('admin.product.create') }}">Dodaj proizvod</a></li>
            <li><a href="{{ route('admin.product.index') }}">Lista proizvoda</a></li>
        </ul>
    </li>
    <li>
        <a href="javascript:;"><i class="fa fa-briefcase"></i> <span>Specifikacije proizvoda</span></a>
        <ul class="acc-menu">
            <li><a href="{{ route('admin.specification.index') }}">Lista specifikacija</a></li>
            <li><a href="{{ route('admin.specification.create') }}">Dodaj specifikaciju</a></li>
        </ul>
    </li>
    <li class="divider"></li>    
    <li>
        <a href="{{ route('admin.post.edit', 1) }}"><i class="fa fa-home"></i> <span>O kompaniji</span></a></li>
        {{--  <a href="#"><i class="fa fa-home"></i> <span>O kompaniji</span></a></li>  --}}
    </li>
    <li>
        <a href="{{ route('admin.post.edit', 2) }}"><i class="fa fa-home"></i> <span>O Takeuchi Global</span></a></li>
        {{--  <a href="#"><i class="fa fa-home"></i> <span>O Takeuchi Global</span></a></li>  --}}
    </li>
    <li>
        <a href="{{ route('admin.gallery.edit', 1) }}"><i class="fa fa-image"></i> <span>Galerija</span></a></li>
    </li>
    <li class="divider"></li>
    <li>
        <a href="{{ route('admin.post.edit', 3) }}"><i class="fa fa-home"></i> <span>Servis i rental</span></a></li>
        {{--  <a href="#"><i class="fa fa-home"></i> <span>Servis i rental</span></a></li>  --}}
    </li>
</ul>
<!-- END SIDEBAR MENU -->
</nav>