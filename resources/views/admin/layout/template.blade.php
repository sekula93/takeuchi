<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Avant</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Avant">
	<meta name="author" content="The Red Team">

    <!-- <link href="/assets/admin/less/styles.less" rel="stylesheet/less" media="all">  -->
    <link rel="stylesheet" href="/assets/admin/css/styles.css?=140">
    <link rel="stylesheet" href="/assets/admin/css/main.css">
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">
	
    <link href='/assets/admin/demo/variations/default.css' rel='stylesheet' type='text/css' media='all' id='styleswitcher'> 
    <link href='/assets/admin/demo/variations/default.css' rel='stylesheet' type='text/css' media='all' id='headerswitcher'> 
    
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries. Placeholdr.js enables the placeholder attribute -->
	<!--[if lt IE 9]>
        <link rel="stylesheet" href="/assets/admin/css/ie8.css">
		<script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
        <script type="text/javascript" src="/assets/admin/plugins/charts-flot/excanvas.min.js"></script>
	<![endif]-->

	<!-- The following CSS are included as plugins and can be removed if unused-->

<link rel='stylesheet' type='text/css' href='/assets/admin/plugins/codeprettifier/prettify.css' /> 
<link rel='stylesheet' type='text/css' href='/assets/admin/plugins/form-toggle/toggles.css' /> 
<link rel="stylesheet" href="/assets/admin/plugins/jquery-fileupload/css/jquery.fileupload-ui.css">


<!-- <script type="text/javascript" src="/assets/admin/js/less.js"></script> -->
<script type="text/javascript">
	var basePath = '{{ url("") }}';
	var csrf = '{{ csrf_token() }}';
	var appLocale = '{{ App::getLocale() }}';
</script>
</head>

<body class="">


    

    <header class="navbar navbar-inverse navbar-fixed-top" role="banner">
        <a id="leftmenu-trigger" class="tooltips" data-toggle="tooltip" data-placement="right" title="Toggle Sidebar"></a>
        <ul class="nav navbar-nav pull-right toolbar">
        	<li class="dropdown">
        		<a href="#" class="dropdown-toggle username" data-toggle="dropdown"><span class="hidden-xs">{{ \Auth::user() ? \Auth::user()->name : 'jos nista' }} <i class="fa fa-caret-down"></i></span></a>
        		<ul class="dropdown-menu userinfo arrow">
        			<li class="userlinks">
        				<ul class="dropdown-menu">
        					<li><a href="#">Uredi profil <i class="pull-right fa fa-pencil"></i></a></li>
        					<li class="divider"></li>
        					<li><a href="{{ route('admin.auth.logout') }}" class="text-right">Odjavi se</a></li>
        				</ul>
        			</li>
        		</ul>
        	</li>
		</ul>
    </header>

    <div id="page-container">
       @include('admin.layout._partials.left-sidebar')

<div id="page-content">
	<div id='wrap'>
		<div id="page-heading">
			{{--  <ol class="breadcrumb">
				<li><a href="index.htm">Dashboard</a></li>
				<li>Extras</li>
			</ol>  --}}

			<h1>@yield('page-heading')</h1>
			{{--  <div class="options">
                <div class="btn-toolbar">
                    <div class="btn-group hidden-xs">
                        <a href='#' class="btn btn-default dropdown-toggle" data-toggle='dropdown'><i class="fa fa-cloud-download"></i><span class="hidden-sm"> Export as  </span><span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Text 	 (*.txt)</a></li>
                            <li><a href="#">Excel File (*.xlsx)</a></li>
                            <li><a href="#">PDF File (*.pdf)</a></li>
                        </ul>
                    </div>
                    <a href="#" class="btn btn-default"><i class="fa fa-cog"></i></a>
                </div>
            </div>  --}}
		</div>
		<div class="container">

			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-primary">
                        @yield('content')
					</div>
				</div>
			</div>
		</div> <!-- container -->
	</div> <!--wrap -->
</div> <!-- page-content -->

    <footer role="contentinfo">
        <div class="clearfix">
            <ul class="list-unstyled list-inline">
                <li>Takeuchi &copy; 2018</li>
                <button class="pull-right btn btn-inverse-alt btn-xs hidden-print" id="back-to-top"><i class="fa fa-arrow-up"></i></button>
            </ul>
        </div>
    </footer>

</div> <!-- page-container -->

<!--
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<script>!window.jQuery && document.write(unescape('%3Cscript src="/assets/admin/js/jquery-1.10.2.min.js"%3E%3C/script%3E'))</script>
<script type="text/javascript">!window.jQuery.ui && document.write(unescape('%3Cscript src="/assets/admin/js/jqueryui-1.10.3.min.js'))</script>
-->

<script type='text/javascript' src='/assets/admin/js/jquery-1.10.2.min.js'></script> 
<script type='text/javascript' src='/assets/admin/js/jqueryui-1.10.3.min.js'></script> 
<script type='text/javascript' src='/assets/admin/js/bootstrap.min.js'></script> 
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.0.0/bootbox.min.js"></script>
<script type='text/javascript' src='/assets/admin/js/enquire.js'></script> 
<script type='text/javascript' src='/assets/admin/js/jquery.cookie.js'></script> 
<script type='text/javascript' src='/assets/admin/js/jquery.nicescroll.min.js'></script> 
<script type='text/javascript' src='/assets/admin/plugins/codeprettifier/prettify.js'></script> 
<script type='text/javascript' src='/assets/admin/plugins/easypiechart/jquery.easypiechart.min.js'></script> 
<script type='text/javascript' src='/assets/admin/plugins/sparklines/jquery.sparklines.min.js'></script> 
<script type='text/javascript' src='/assets/admin/plugins/form-toggle/toggle.min.js'></script> 
<script type='text/javascript' src='/assets/admin/plugins/jquery-fileupload/js/jquery.iframe-transport.js'></script> 
<script type='text/javascript' src='/assets/admin/plugins/jquery-fileupload/js/jquery.fileupload.js'></script> 
<script type='text/javascript' src='/assets/admin/js/placeholdr.js'></script> 
<script type='text/javascript' src='/assets/admin/js/application.js'></script> 
<script type='text/javascript' src='/assets/admin/demo/demo.js'></script>
<script type="text/javascript" src='/assets/admin/ckeditor/ckeditor.js'></script>
<script type='text/javascript' src='/assets/admin/js/file-upload.js'></script> 
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js'></script> 
<script type='text/javascript' src='/assets/admin/js/app.js'></script> 

</body>
</html>