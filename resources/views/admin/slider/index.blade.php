@extends('admin.layout.template')

@section('page-heading', 'Blog')

@section('content')
    @include('flash::message')
    <div class="panel-heading">
        <h4>Dashboard</h4>
        <div class="options">

        </div>
    </div>
    <div class="panel-body resource-container" data-resource="slider" data-modal="Da li želiš da obrišeš slajd?">
        <div class="collection">
            @if(count($listArray))
            <ul class="gallery slide-gallery">
                @foreach($listArray as $item)
                    <li class="list-item" data-id="{{ $item->id }}" id="image-{{ $item->id }}">
                        <img src="{{ config('settings.image.slide.upload_dir').$item->filename }}" alt="">
                        <div style="margin-top: 10px">
                            <div class="col-md-3" >
                                <div class="toggle toggle-success {{ $item->is_active ? 'active' : '' }} table-ajax col-md-3" title="Status" data-id="{{ $item->id }}"></div>
                            </div>
                            <div class="col-md-offset-6 col-md-1">
                                <a href="{{ route('admin.slider.edit', $item->id) }}" class="option-edit col-md-3"><i class="fa fa-pencil"></i></a>
                            </div>
                            <div class="col-md-1" >
                                <a style="margin-left: 15px; cursor: pointer; color: red;" data-parent="li" class="delete-item"><i class="fa fa-times"></i> </a>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
            @else 
                <h2>Lista je trenutno prazna</h2>
            @endif
        </div>
    </div>
@stop