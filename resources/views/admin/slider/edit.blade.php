@extends('admin.layout.template')

@section('page-heading', 'Slider')

@section('content')
    <div class="panel-heading">
        <h4>Novi slider</h4>
        <div class="options">

        </div>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" action="{{ route('admin.slider.update', $slide->id) }}" method="post" enctype="multipart/form-data">
            <div class="col-md-6" >
                {{ csrf_field() }}
                {{ method_field('put') }}

                <div class="form-group">
                    <label for="title" class="col-sm-3 control-label">Naslov</label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="title" name="title">{{ $slide->title }}</textarea>
                    </div>
                    <div class="col-sm-3">
                        <p class="help-block">{{ $errors->first('title') }}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="subtitle" class="col-sm-3 control-label">Podnaslov</label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="subtitle" name="subtitle">{{ $slide->subtitle }}</textarea>
                    </div>
                    <div class="col-sm-3">
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="link" class="col-sm-3 control-label">Link</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="link" name="link" value="{{ $slide->link }}">
                    </div>
                    <div class="col-sm-3">
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="form-group">
                </div>
                <div class="form-group">
                    <div class="col-md-4 col-md-offset-3">
                        <input type="hidden" name="is_active" value="0">
                        <div class="toggle toggle-success {{ $slide->is_active ? 'active' : '' }} form-active" title="Status" data-id="{{ $slide->id }}"></div>
                    </div>
                    <div class="col-sm-3">
                        <button class="btn-primary btn">Snimi</button>                    
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="col-sm-9">  
                        <div class="fileinput fileinput-exists" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 360px; height: 240px;"></div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 360px; max-height:auto;">
                                <img src="{{ config('settings.image.slide.upload_dir').$slide->filename }}" class="img-responsive" alt="">
                            </div>
                            <div>
                                <span class="btn btn-default btn-file"><span class="fileinput-new">Izaberi sliku</span><span class="fileinput-exists">Izmeni</span>
                                <input type="file" name="file" accept="image/*"></span>
                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Izbriši</a>
                            </div>
                        </div>
                        <p class="help-block">{{ $errors->first('file') }}</p> 
                        <p>Preporučena dimenzija: <b>1920x800px<b> (do 2mb)</p>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop