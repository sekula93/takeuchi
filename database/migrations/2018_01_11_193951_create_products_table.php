<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_subcategory_id')->unsigned();
            $table->integer('type')->default(1);
            $table->string('slug', 100);
            $table->string('title');
            $table->text('text');
            $table->tinyInteger('is_active');
            $table->integer('order')->default(0);
            $table->string('filename', 50)->nullable();
            $table->string('attachment', 50)->nullable();
            $table->text('attachment_label');

            $table->foreign('product_subcategory_id')->references('id')->on('product_subcategories')->onDelete('cascade')->onUpdate('cascade');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
