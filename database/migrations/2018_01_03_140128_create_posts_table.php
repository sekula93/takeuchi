<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_category_id')->unsigned();
            $table->string('title');
            $table->string('slug');
            $table->text('text');
            $table->text('excerpt');
            $table->string('filename');
            $table->string('document_label')->nullable();
            $table->string('document_filename')->nullable();
            $table->tinyInteger('is_active');
            $table->tinyInteger('flag')->default(1);
            $table->tinyInteger('order')->default(0);

            $table->foreign('post_category_id')->references('id')->on('post_categories')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
