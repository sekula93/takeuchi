<?php 

    return [
        'app_name' => 'Takeuchi - Srbija',
        'image' => [
            'upload_path' =>  public_path().'/uploads/',
            'slide' => [
                'quality' => 95,
                'upload_dir' => '/uploads/slider/images/',
                'full_destination_filename' => false,
                'random_name' => true,
                'resize_original' => true,
                'resize_dimensions' => [1920,800, true],
                'create_thumbs' => false,
                'thumb_dimensions' => []
            ],
            'post' => [
                'quality' => 95,
                'upload_dir' => '/uploads/post/images/',
                'full_destination_filename' => false,
                'random_name' => true,
                'resize_original' => true,
                'resize_dimensions' => [1300,700, true],
                'create_thumbs' => false,
                'thumb_dimensions' => [],
                'slider' => [
                    'quality' => 95,
                    'upload_dir' => '/uploads/posts/images/',
                    'full_destination_filename' => false,
                    'random_name' => true,
                    'resize_original' => false,
                    'resize_dimensions' => [1300, 700, true],
                    'create_thumbs' => false,
                    'thumb_dimensions' => []
                ]
            ],
            'product' => [
                'quality' => 95,
                'upload_dir' => '/uploads/product/images/',
                'full_destination_filename' => false,
                'random_name' => true,
                'resize_original' => false,
                'resize_dimensions' => [1920,1080],
                'create_thumbs' => false,
                'thumb_dimensions' => [],
                'slider' => [
                    'quality' => 95,
                    'upload_dir' => '/uploads/products/images/',
                    'full_destination_filename' => false,
                    'random_name' => true,
                    'resize_original' => false,
                    'resize_dimensions' => [1200, 450],
                    'create_thumbs' => false,
                    'thumb_dimensions' => []
                ]
            ],
            'product-category' => [
                'quality' => 95,
                'upload_dir' => '/uploads/product-category/images/',
                'full_destination_filename' => false,
                'random_name' => true,
                'resize_original' => true,
                'resize_dimensions' => [1920,1080],
                'create_thumbs' => false,
                'thumb_dimensions' => [],
                'slider' => [
                    'quality' => 95,
                    'upload_dir' => '/uploads/product-categories/images/',
                    'full_destination_filename' => false,
                    'random_name' => true,
                    'resize_original' => false,
                    'resize_dimensions' => [1200, 450],
                    'create_thumbs' => false,
                    'thumb_dimensions' => []
                ]
            ],
            'associates' => [
                'quality' => 95,
                'upload_dir' => '/uploads/associates/images/',
                'full_destination_filename' => false,
                'random_name' => true,
                'resize_original' => true,
                'resize_dimensions' => [100,60],
                'create_thumbs' => false,
                'thumb_dimensions' => []
            ],
            'gallery' => [
                'quality' => 95,
                'upload_dir' => '/uploads/gallery/images/',
                'full_destination_filename' => false,
                'random_name' => true,
                'resize_original' => false,
                'resize_dimensions' => [1920,1080],
                'create_thumbs' => false,   
                'thumb_dimensions' => []
            ],
        ],
        'file' => [
            'product' => [
                'upload_dir' => '/uploads/product/documents/',
                'random_name' => true
            ],
        ],
    ];