$(document).ready(function(){

     var FadeTransition = Barba.BaseTransition.extend({
            start: function() {
                /**
                * This function is automatically called as soon the Transition starts
                * this.newContainerLoading is a Promise for the loading of the new container
                * (Barba.js also comes with an handy Promise polyfill!)
                */

                // As soon the loading is finished and the old page is faded out, let's fade the new page
                $("html, body").animate({ scrollTop: 0 }, "slow");
                Promise
                .all([this.newContainerLoading, this.fadeOut()])
                .then(this.fadeIn.bind(this));
                $('#preloader').css('visibility', 'visible');
                $('#preloader').css('opacity', 1);
            },

            fadeOut: function() {
                /**
                * this.oldContainer is the HTMLElement of the old Container
                */

                return $(this.oldContainer).animate({ opacity: 0 }).promise();
            },

            fadeIn: function() {
                /**
                * this.newContainer is the HTMLElement of the new Container
                * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
                * Please note, newContainer is available just after newContainerLoading is resolved!
                */

                var _this = this;
                var $el = $(this.newContainer);

                $(this.oldContainer).hide();

                $el.css({
                    visibility : 'visible',
                    opacity : 0
                });

                $el.animate({ opacity: 1 }, 500, function() {
                    /**
                    * Do not forget to call .done() as soon your transition is finished!
                    * .done() will automatically remove from the DOM the old Container
                    */
                    $('#preloader').css('visibility', 'hidden');
                    $('#preloader').css('opacity', 0);
                   
                    _this.done();
                });
            }
            });

            /**
            * Next step, you have to tell Barba to use the new Transition
            */

            Barba.Pjax.getTransition = function() {
                /**
                * Here you can use your own logic!
                * For example you can use different Transition based on the current page or link...
                */
                
                return FadeTransition;
            };

    Gallery.init();
    Barba.Pjax.start();
    Barba.Dispatcher.on('newPageReady', function(currentStatus, prevStatus) {
      initMainSlider();
      initPostSlider();
      initCompatibleSlider();
      displaySpecifications();
      initDatepicker();
      removeErrorClass();

      $('.compatible-slider .slick-prev').html('<i class="fa fa-chevron-left">');
      $('.compatible-slider .slick-next').html('<i class="fa fa-chevron-right">');
    });

    Barba.Dispatcher.on('transitionCompleted', function(currentStatus, prevStatus) {
        initMainSlider();
        initPostSlider();
        initCompatibleSlider();
        displaySpecifications();
        initDatepicker();
        removeErrorClass();
  
        $('.compatible-slider .slick-prev').html('<i class="fa fa-chevron-left">');
        $('.compatible-slider .slick-next').html('<i class="fa fa-chevron-right">');
    });
      
    initMainSlider();
    initPostSlider();
    initCompatibleSlider(); 
    initLightGallery();
    displaySpecifications();
    initDatepicker();
    removeErrorClass();

    $('.compatible-slider .slick-prev').html('<i class="fa fa-chevron-left">');
    $('.compatible-slider .slick-next').html('<i class="fa fa-chevron-right">');
    
    // ready  
  });

function removeErrorClass() {
  $('input, textarea').keyup(function() {
    $(this).removeClass('error');
  })
}
function initDatepicker() {
  $('input[name="daterange"]').daterangepicker({
    autoApply: true,
    autoUpdateInput: true,
    locale: {
      applyLabel: 'Primeni',
      cancelLabel: 'Odustani',
      format: 'DD.MM.YYYY.',
      weekLabel: "N",
      daysOfWeek: [
          "Ned",
          "Pon",
          "Uto",
          "Sre",
          "Čet",
          "Pet",
          "Sub"
      ],
      monthNames: [
          "Januar",
          "Februar",
          "Mart",
          "April",
          "Maj",
          "Jun",
          "Jul",
          "Avgust",
          "Septembar",
          "Oktobar",
          "Novembar",
          "Decembar"
      ],
      firstDay: 1
    },
    
  });
}

var Gallery = Barba.BaseView.extend({
  namespace: 'front.gallery.index',
  onEnter: function() {
    
    initLightGallery();
  
  },
});



function initLightGallery() {
  if ($("#lightgallery").length) {
    $("#lightgallery").lightGallery({
      thumbnail: true
    }); 
  }
}

function displaySpecifications() {
  $('.single-product-table').first().css('display', 'flex');
    $('.specification-tabs button').first().addClass('active');

    $('.compatible-slider').css('display', 'none');
    
    $('.slider-tab-1').css('display', 'block');
}

function initMainSlider() {
  $('.slider').slick({
    dots: false,
    autoplay: true,
    prevArrow: $('.slick-prev'),
    nextArrow: $('.slick-next'),
  });
}

function initPostSlider() {
  $('.post-slider').slick({
    dots: false,
    prevArrow: $('.slick-prev'),
    nextArrow: $('.slick-next'),
  });
}

function initCompatibleSlider() {
  

  $('.compatible-slider').slick({
    dots: false,
    arrows: true,  
    // prevArrow: $('.slick-prev'),
    // nextArrow: $('.slick-next'),
    slidesToShow: 5,
    autoplay: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
})
}

$(document).on('click', '.list-item', function() {
    $(this).addClass('active');
    $(this).find('.sub-list').addClass('active');
})

$(document).on('click', '.list-item.active', function() {
    $(this).find('.sub-list').removeClass('active');
    $(this).removeClass('active');
})

function initMap() {
    // Styles a map in night mode.
    var map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: 45.294, lng: 19.855},
      zoom: 11,
      styles: [
        {
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#212121"
            }
          ]
        },
        {
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#757575"
            }
          ]
        },
        {
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#212121"
            }
          ]
        },
        {
          "featureType": "administrative",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#757575"
            }
          ]
        },
        {
          "featureType": "administrative.country",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#9e9e9e"
            }
          ]
        },
        {
          "featureType": "administrative.land_parcel",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "administrative.locality",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#bdbdbd"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#757575"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#181818"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#616161"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1b1b1b"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#2c2c2c"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#8a8a8a"
            }
          ]
        },
        {
          "featureType": "road.arterial",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#373737"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#3c3c3c"
            }
          ]
        },
        {
          "featureType": "road.highway.controlled_access",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#4e4e4e"
            }
          ]
        },
        {
          "featureType": "road.local",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#616161"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#757575"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#000000"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#3d3d3d"
            }
          ]
        }
      ]
    });
    
      var marker = new google.maps.Marker({
          position: {lat: 45.354, lng: 19.830},
          map: map
      });

      var contentString = 'Takeuchi - Srbija';

      var infowindow = new google.maps.InfoWindow({
        content: contentString
      });
      
      infowindow.open(map, marker);

      marker.addListener('click', function() {
          infowindow.open(map, marker);
      });

      $(document).on('click', '.specification-tabs button', function() {
        var element = $(this);
        var curTab = element.data('uid');
        console.log(curTab);

        $('.specification-tabs button').removeClass('active');
        element.addClass('active');

        $('.single-product-table').css('display', 'none');
        $('.'+curTab).css('display', 'flex');

        $('.compatible-slider').css('display', 'none');
        $('.slider-'+curTab).css('display', 'block');
        $('.compatible-slider').slick('setPosition');


      })

  }

  $(document).on('click', '.nav-item', function() {
    $('.nav-item').removeClass('active');
    $(this).addClass('active');
    $('.navbar-collapse').removeClass('show');
  })

  $(document).on('change', '#attachment', function() {
    var file = $(this)[0].files[0];
    if (file) {
      $('.attach-name').text(file.name)
    } else {
      $('.attach-name').text('')
    }   
  })

  $(document).on('click', '.career-btn', function(event) {
      event.preventDefault();
      console.log('click validation');

      if (isFormValid()) {
        return false;
      } else {
        return true;
      }
      
      
  });


$(document).on('click', '.contact-btn', function(event) {
    event.preventDefault();
    console.log('click validation');
    var form = $('#needs-validation');
    console.log(form.serialize());
    // if (isFormValid()) {
    //     return false;
    // } else {
    //     console.log('ovde je email');
        
        $.ajax({
            type: "post",
            url: "/send",
            data: form.serialize(),
            dataType: "json",
            success: function (response) {
                if (response.status == "success") {
                    console.log('success email');
                    $('.message').text('Poruka uspešno poslata!');
                    form[0].reset();
                    
                } else {
                    $('.message').text('Došlo je do greške. Pokušajte kasnije, hvala.');
                    form[0].reset();
                    
                }
            },
            error: function(error) {
                $('.message').text('Došlo je do greške. Pokušajte kasnije, hvala.');
                form[0].reset();
            }
        });
    // }
    
});

  $(document).on('click', '.contact-info-btn', function(event) {
    event.preventDefault();
    console.log('click validation');

    if (isFormValid()) {
      return false;
    } else {
      return true;
    }
    
  });


  $(document).on('click', '.share .fa', function() {
    var link = window.location.href
    FB.ui({
      method: 'share',
      href: link,
    }, function(response){});
  })


function isFormValid() {
  var name = $('input[name="name"]');
  var email = $('input[name="email"]');
  var text = $('textarea[name="text"]');
  // var file = $('input[name="attachment"]')[0].files[0];
  var error = 0;

  if (!name.val() || name.val() == '') {
    console.log('error name');
    error++
    name.addClass('error');
  }
  
  if (!email.val() || email.val() == '') {
    console.log('error email');
    error++
    email.addClass('error');
  }
  
  if (!text.val() || text.val() == '') {
    console.log('error text');
    error++
    text.addClass('error');
  }

  return error;

}

 