$(document).on('click', '.career-btn', function() {
    var button = $(this);
    var form = button.parents('form')[0];
    var formData = new FormData(form);

    $.ajax({
        type: "POST",
        url: "/send",
        data: formData,
        dataType: "json",
        processData: false,
        contentType: false,
        success: function (response) {
            if (response.status == "success") {
                console.log('success email');
                $('.message').text('Poruka uspešno poslata!');
                form.reset();
                
            } else {
                $('.message').text('Došlo je do greške. Pokušajte kasnije, hvala.');
                form.reset();
                
            }
        },
        error: function(error) {
            $('.message').text('Došlo je do greške. Pokušajte kasnije, hvala.');
            form.reset();
        }
    });
})

$(document).on('click', '.rental-btn', function(event) {
    event.preventDefault();
    var form = $('#needs-validation');
    console.log(form.serialize());
        
        $.ajax({
            type: "post",
            url: "/send",
            data: form.serialize(),
            dataType: "json",
            success: function (response) {
                if (response.status == "success") {
                    console.log('success email');
                    $('.message').text('Poruka uspešno poslata!');
                    form[0].reset();
                    
                } else {
                    $('.message').text('Došlo je do greške. Pokušajte kasnije, hvala.');
                    form[0].reset();
                    
                }
            },
            error: function(error) {
                $('.message').text('Došlo je do greške. Pokušajte kasnije, hvala.');
                form[0].reset();
            }
        });
    // }
    
});