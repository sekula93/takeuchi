$(document).ready(function () {
    assignToggle();

    if ($('textarea[name="text"]').length) {
        CKEDITOR.replace( 'text' );
    }


    // Reorder
    $('table.sortable tbody').sortable({
        placeholder: '',
        helper: fixWidthHelper,
        handle: ".bars",
        stop: function(e, ui) {
            var data = $(this).sortable('serialize', {key: "sort_item[]"});
            var resource = $('.resource-container').data('resource');
            console.log('stop, ajax');
            $.ajax({
                method: 'put',
                url: basePath+'/admin/'+resource+'/reorder',
                dataType: 'json',
                data: data+"&_token="+csrf,
                success: function(response) {
                    console.log(response);
                }
            });
        }
    }).disableSelection();

    // multiselect related products
    $('#related_products').multiselect({
        numberDisplayed: 0
    });
});

//toggle create/edit form
$(document).on('toggle', '.toggle.form-active', function(e, active) {
    var button = $(this);
    var activeInput = button.siblings('input[name="is_active"]');
    if (active) {
        activeInput.val(1);
    } else {
        activeInput.val(0);
    }
    console.log('form toggle', activeInput, activeInput.val());
});

//toggle table index
$(document).on('toggle', '.toggle.table-ajax', function(e, active) {
    var button = $(this);
    var resource = button.parents('.resource-container').data('resource');
    var itemId = button.data('id');
    var URL = basePath+'/admin/'+resource+'/change-status';

    $.ajax({
        type: 'post',
        url: URL,
        dataType: 'json',
        data: { id : itemId, _token: csrf },
        success: function(response) {
            if(response.state == 1) {
                button.parents('tr').children('td:not(.table-options)').removeClass('inactive');
            } else {
                button.parents('tr').children('td:not(.table-options)').addClass('inactive');
            }
        }
    });
});

// delete item
$(document).on('click', '.delete-item', function(e){
    e.preventDefault();
    var button = $(this);
    var parent;
    if (button.data('parent') == 'li') {
        parent = 'li';
    } else {
        parent = 'tr';
    }
    deleteItem(button, parent);
});


// slugify
$(document).on('keyup', '.slugify', function(e) {
    var postId = $(this).data('id');

    var element = $(this);
    var resource = element.parents('form').data('resource');
    console.log(resource);
    $.ajax({
        type: 'POST',
        url: basePath+'/admin/'+resource+'/slugify',
        dataType: 'json',
        data: {title: $(this).val(), id: postId, _token: csrf},
        success: function(response) {
            element.parents('form').find('input[name*=slug]').val(response.slug);
        },
        error: function(response) {
            element.parents('form').find('input[name*=slug]').val('');
        }
    });
});

function assignToggle(){
    $('.toggle').toggles({on:false});
    $('.toggle.active').toggles({on:true});
}

function deleteItem(button, parent) {
    var resource = button.parents('.resource-container').data('resource');
    var modalText = button.parents('.resource-container').data('modal');
    var itemId = button.parents(parent).data('id');

    var URL = basePath+'/admin/'+resource+'/'+itemId;
    var row = button.parents(parent);
    bootbox.confirm(modalText, function(confirmed){
        if(confirmed) {
            $.ajax({
                type: 'DELETE',
                url: URL,
                data: {_token: csrf},
                dataType: 'json',
                success: function(response) {
                    if(response.status == 'success') {
                        row.hide(400, function(){
                            row.remove();
                        });
                    }
                }
            });
        }
    });
}

// Reorder
$('.gallery.slide-gallery').sortable({
    placeholder: 'sortable-placeholder list-item',
    stop: function(e, ui) {
        var data = $(this).sortable('serialize', {key: "sort_item[]"});
        console.log(data);
        $.ajax({
            method: 'put',
            url: basePath+'/admin/slider/reorder',
            dataType: 'json',
            data: data+"&_token="+csrf,
            success: function(response) {
                console.log(response);
            }
        });
    }
});
$('.gallery').disableSelection();


function fixWidthHelper(e, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
}